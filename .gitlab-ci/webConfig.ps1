function Update-Config {
    param (
        $Path,
        $KeyVaultUrl,
        $SubjectDN
    )

    $config = (Get-Content $Path) -as [Xml] 
    $environmentVariables = $config.CreateNode("element", "environmentVariables", $null)
    $endpoint = $config.CreateNode("element", "environmentVariable", $null)
    $endpoint.SetAttribute("name", "Azure:KeyVault:Endpoint")
    $endpoint.SetAttribute("value", $KeyVaultUrl)
    $environmentVariables.AppendChild($endpoint)
    $dn = $config.CreateElement("environmentVariable")
    $dn.SetAttribute("name", "Azure:KeyVault:SubjectDistinguishedName")
    $dn.SetAttribute("value", $SubjectDN)
    $environmentVariables.AppendChild($dn)
    $config.configuration.location.'system.webServer'.aspNetCore.AppendChild($environmentVariables)
    $config.Save($Path)
}

