using AAE.GGTool.Domain.SeedWork;
using MediatR;

namespace AAE.GGTool.Application.Configuration.DomainEvents
{
    public interface IDomainEventHandler<in TDomainEvent> :
        INotificationHandler<TDomainEvent> where TDomainEvent : IDomainEvent
    {
    }
}
