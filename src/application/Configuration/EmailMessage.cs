using MimeKit;
using System.Collections.Generic;

namespace AAE.GGTool.Application.Configuration
{
    /// <summary>
    /// Data used to create an outgoing email message
    /// </summary>
    public class EmailMessage
    {
        public IEnumerable<MailboxAddress> To { get; set; } = new List<MailboxAddress>();
        public MailboxAddress From { get; set; }
        public string Subject { get; set; }
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }
    }
}
