using System.Reflection;

namespace AAE.GGTool.Application;

/// <summary>
/// Used for reading embedded resources from class library.
/// </summary>
public static class EmbeddedResource
{
    /// <summary>
    /// Returns a string that has been marked as an embedded resource in this assembly.
    /// </summary>
    public static async Task<string> GetStringAsync(string name)
    {
        return await GetStringAsync(typeof(EmbeddedResource).Assembly, name);
    }

    private static StreamReader? GetStream(Assembly assembly, string name)
    {
        foreach (string resName in assembly.GetManifestResourceNames())
        {
            if (resName.EndsWith($".{name}"))
            {
                return new StreamReader(
                    assembly.GetManifestResourceStream(resName) ?? throw new Exception("Resource not found"));
            }
        }

        return null;
    }

    private static async Task<string> GetStringAsync(Assembly assembly, string name)
    {
        StreamReader? sr = GetStream(assembly, name) ?? throw new Exception($"Error reading {name}");
        var data = string.Empty;

        try
        {
            data = await sr.ReadToEndAsync();
        }
        catch (Exception ex)
        {
            throw new Exception($"Missing resource: {name}", ex);
        }
        finally
        {
            sr.Close();
        }

        return data;
    }
}
