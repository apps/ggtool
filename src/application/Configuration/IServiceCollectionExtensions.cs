﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IServiceCollectionExtensions
    {
        public static IEnumerable<Type> GetImplementationTypes<TService>(this IServiceCollection services)
            where TService : class
        {
            return GetImplementationTypes(services, typeof(TService));
        }

        public static IEnumerable<Type> GetImplementationTypes(this IServiceCollection services, Type serviceType)
        {
            if (services is null)
                throw new ArgumentNullException(nameof(services));

            if (serviceType is null)
                throw new ArgumentNullException(nameof(serviceType));

            return services
                .Where(d => d.ServiceType == serviceType && d.ImplementationType != null)
                .Select(d => d.ImplementationType);
        }
    }
}