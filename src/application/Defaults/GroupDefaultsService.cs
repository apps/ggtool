using AAE.GGTool.Repositories;
using System;
using System.Threading.Tasks;

namespace AAE.GGTool.Services
{
    public class GroupDefaultsService : IGroupDefaultsService
    {
        private readonly IGroupDefaultsRepository _groupDefaultsRepository;

        public GroupDefaultsService(IGroupDefaultsRepository groupDefaultsRepository)
        {
            _groupDefaultsRepository = groupDefaultsRepository ?? throw new ArgumentNullException(nameof(groupDefaultsRepository));
        }
        public Task<GroupDefault> GetAsync(int id)
        {
            return _groupDefaultsRepository.GetAsync(id);
        }

        public Task UpdateAsync(GroupDefault defaults)
        {
            return _groupDefaultsRepository.UpdateAsync(defaults);
        }
    }
}
