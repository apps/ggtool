using System.Threading.Tasks;

namespace AAE.GGTool.Repositories
{
    public interface IGroupDefaultsRepository
    {
        /// <summary>
        /// Updates existing group defaults in the repository
        /// </summary>
        Task UpdateAsync(GroupDefault defaults);

        /// <summary>
        /// Gets group defaults for a specific id
        /// </summary>
        Task<GroupDefault> GetAsync(int id);
    }
}
