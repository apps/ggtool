namespace AAE.GGTool.Enum
{
    public enum DeliverySettings
    {
        ALL_MAIL,
        DAILY,
        DIGEST,
        DISABLED,
        NONE
    }
}
