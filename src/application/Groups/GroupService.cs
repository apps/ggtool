using AAE.GGTool.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAE.GGTool.Services
{
    public class GroupService : IGroupService
    {
        private readonly IGroupRepository _groupRepository;

        public GroupService(IGroupRepository groupRepository)
        {
            _groupRepository = groupRepository ?? throw new ArgumentNullException(nameof(groupRepository));
        }

        public Task<Group> AddAsync(Group group)
        {
            return _groupRepository.AddAsync(group);
        }

        public Task DeleteAsync(string groupEmail)
        {
            return _groupRepository.DeleteAsync(groupEmail);
        }

        public Task<Group> GetAsync(string groupEmail, bool bypassCache = false)
        {
            return _groupRepository.GetAsync(groupEmail, bypassCache);
        }

        public Task<IList<string>> ListAsync()
        {
            return _groupRepository.ListAsync();
        }

        public Task<Group> UpdateAsync(Group group)
        {
            return _groupRepository.UpdateAsync(group);
        }
    }
}
