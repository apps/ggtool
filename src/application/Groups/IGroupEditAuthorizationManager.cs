using System.Threading.Tasks;

namespace AAE.GGTool.Application.Groups
{
    public interface IGroupEditAuthorizationManager
    {
        /// <summary>
        /// Whether the specified NetID has edit privileges on the specified group in the specified app
        /// </summary>
        /// <param name="netid"></param>
        /// <param name="group"></param>
        /// <param name="uwAppId"></param>
        /// <returns></returns>
        Task<bool> CanEditAysnc(string netid, string group, string uwAppId);
    }
}
