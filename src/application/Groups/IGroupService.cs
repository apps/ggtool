using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAE.GGTool.Services
{
    public interface IGroupService
    {
        /// <summary>
        /// Add a new Google Group
        /// </summary>
        /// <param name="group"></param>
        Task<Group> AddAsync(Group group);

        /// <summary>
        /// Updates an existing Google Group
        /// </summary>
        Task<Group> UpdateAsync(Group group);

        /// <summary>
        /// Gets an existing Google Group
        /// </summary>
        /// <param name="groupEmail">group email identitier in the form group@domain</param>
        /// <returns></returns>
        Task<Group> GetAsync(string groupEmail, bool bypassCache = false);

        /// <summary>
        /// Delete this specified Google Group
        /// </summary>
        Task DeleteAsync(string groupEmail);

        /// <summary>
        /// List all groups for this app
        /// </summary>
        Task<IList<string>> ListAsync();
    }
}
