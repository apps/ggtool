using System.Collections.Generic;
using System.Linq;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Application.Import;

public class BatchGroupMemberCollection
{
    public BatchGroupMemberCollection(MemberCompareResult compareResult)
    {
        MembersToAdd = compareResult.MembersToAdd;
        MembersToUpdate = compareResult.MembersToUpdate.Select(m => m.Item2).ToList();
        ExtraMembers = compareResult.ExtraMembers;
    }

    public IList<Member> MembersToAdd { get; }
    public IList<Member> ExtraMembers { get; }
    public IList<Member> MembersToUpdate { get; }
}
