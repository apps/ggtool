using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Import;

public interface IImportFileProcessor
{
    /// <summary>
    /// Process an import file to get a list of group members
    /// </summary>
    /// <param name="file">A CSV or Excel file containing a member list</param>
    /// <param name="contentType">MIME encoding to specify file type</param>
    IList<Member> ProcessFile(byte[] file, string contentType);

    /// <summary>
    /// Validates the file
    /// </summary>
    /// <param name="file">A CSV or Excel file containing a member list</param>
    /// <param name="contentType">MIME encoding to specify file type</param>
    /// <remarks>Will return true if the file is a CSV or Excel file, and has at least an email column</remarks>
    bool ValidateFile(byte[] file, string contentType);
}
