using System.Data;
using System.Security.Cryptography;
using System.Text;
using ExcelDataReader;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Import;

public class ImportFileProcessor : IImportFileProcessor
{
    private IDictionary<byte[], DataSet> _cache;

    private const string MIMETypeXLS = "application/vnd.ms-excel";
    private const string MIMETypeXLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private const string MIMETypeCSV = "text/csv";

    private const string ROLE_COLUMN = "ROLE";
    private const string DELIVERY_COLUMN = "DELIVERYSETTINGS";
    private const string MEMBER_COLUMN = "MEMBER";

    private IReadOnlyList<string> RequiredFields = new List<string> { MEMBER_COLUMN };

    public ImportFileProcessor()
    {
        _cache = new Dictionary<byte[], DataSet>();
    }
    public IList<Member> ProcessFile(byte[] file, string contentType)
    {
        if (!VerifyContentType(contentType))
            throw new Exception($"Cannot process a file with content type {contentType}");

        var cacheKey = CacheKey(file);

        if (!_cache.TryGetValue(cacheKey, out DataSet data))
        {
            data = ParseData(file, contentType);
            _cache.Add(cacheKey, data);
        }

        var returnList = new List<Member>();
        DataRow dataRow;
        var hasDeliverySettings = data.Tables[0].Columns.Contains(DELIVERY_COLUMN);
        var hasRole = data.Tables[0].Columns.Contains(ROLE_COLUMN);
        for (var i = 0; i < data.Tables[0].Rows.Count; i++)
        {
            dataRow = data.Tables[0].Rows[i];

            returnList.Add(new Member()
            {
                Email = dataRow[MEMBER_COLUMN].ToString().ToLower(),
                DeliverySettings = hasDeliverySettings ? (string)dataRow[DELIVERY_COLUMN] : null,
                Role = hasRole ? (string)dataRow[ROLE_COLUMN] : Roles.MEMBER.ToString()
            });
        }

        return returnList;
    }

    public bool ValidateFile(byte[] file, string contentType)
    {
        if (!VerifyContentType(contentType))
            throw new Exception($"Cannot validate a file with content type {contentType}");

        var cacheKey = CacheKey(file);

        if (!_cache.TryGetValue(cacheKey, out DataSet data))
        {
            data = ParseData(file, contentType);
            _cache.Add(cacheKey, data);
        }

        var listOfColumnNames = data.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();

        string field;
        for (var i = 0; i < RequiredFields.Count; i++)
        {
            field = RequiredFields[i];
            if (!listOfColumnNames.Contains(field))
                return false;
        }

        return true;
    }

    /// <summary>
    /// Parses the specified file into a <see cref="DataSet"/> based on the ContentType
    /// </summary>
    private static DataSet ParseData(byte[] file, string contentType)
    {
        DataSet data;
        switch (contentType)
        {
            case MIMETypeCSV:
                data = ParseCSVData(file);
                break;
            case MIMETypeXLS:
                data = ParseExcelData(file);
                break;
            case MIMETypeXLSX:
                data = ParseExcelData(file);
                break;
            default:
                throw new Exception($"Unable to parse data for content type {contentType}");
        }

        // capitalize all column heads for use of parseing
        var columns = data.Tables[0].Columns.Cast<DataColumn>();
        foreach(var column in columns)
        {
            column.ColumnName = column.ColumnName.ToUpper();
        }

        return data;
    }

    /// <summary>
    /// Creates a <see cref="DataSet"/> from an XLS file
    /// </summary>
    private static DataSet ParseExcelData(byte[] file)
    {
        using (var ms = new MemoryStream(file))
        {
            using (var reader = ExcelReaderFactory.CreateReader(ms, new ExcelReaderConfiguration()
            {
                FallbackEncoding = Encoding.GetEncoding(1252),
            }))
            {
                var dataSet = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });

                return dataSet;
            }
        }
    }

    /// <summary>
    /// Creates a <see cref="DataSet"/> from a CSV file
    /// </summary>
    private static DataSet ParseCSVData(byte[] file)
    {
        using (var ms = new MemoryStream(file))
        {
            using (var reader = ExcelReaderFactory.CreateCsvReader(ms, new ExcelReaderConfiguration()
            {
                AnalyzeInitialCsvRows = 1
            }))
            {
                var dataSet = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });

                return dataSet;
            }
        }
    }

    private static bool VerifyContentType(string contentType)
    {
        return contentType == MIMETypeCSV || contentType == MIMETypeXLS || contentType == MIMETypeXLSX;
    }

    private static byte[] CacheKey(byte[] file)
    {
        return new MD5CryptoServiceProvider().ComputeHash(file);
    }
}
