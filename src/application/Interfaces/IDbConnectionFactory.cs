﻿using System.Data;

namespace AAE.Data
{
    /// <summary>
    /// Interface for a factory for creating db connections
    /// </summary>
    public interface IDbConnectionFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IDbConnection"/> implementation class.
        /// </summary>
        /// <returns></returns>
        IDbConnection GetDbConnection();

        /// <summary>
        /// Initializes a new instance of the <see cref="IDbDataParameter"/> implementation class.
        /// </summary>
        IDbDataParameter GetDbParameter();

        /// <summary>
        /// Initializes a new instance of the <see cref="IDbDataParameter"/> implementation class that uses the parameter name and the data type.
        /// </summary>
        /// <param name="parameterName"> The name of the parameter to map.</param>
        /// <param name="dbType">One of the <see cref="DbType"/> values.</param>
        IDbDataParameter GetDbParameter(string parameterName, DbType dbType);

        /// <summary>
        /// Initializes a new instance of the <see cref="IDbDataParameter"/> implementation class that uses the parameter name  and a value of the new <see cref="IDbDataParameter"/>.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to map.</param>
        /// <param name="value">An <see cref="System.Object"/> that is the value of the <see cref="IDbDataParameter"/>.</param>
        IDbDataParameter GetDbParameter(string parameterName, object value);

        /// <summary>
        /// Initializes a new instance of the <see cref="IDbDataParameter"/> implementation class that uses the parameter name and the <see cref="DbType"/>, and the size.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to map.</param>
        /// <param name="dbType">One of the <see cref="DbType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        IDbDataParameter GetDbParameter(string parameterName, DbType dbType, int size);
    }
}
