using AAE.GGTool.Application.Configuration;
using MimeKit;
using System.Threading.Tasks;

namespace AAE.GGTool.Application.Interfaces
{
    public interface IEmailService
    {
        /// <summary>
        /// Sends a message via email
        /// </summary>
        /// <returns>If the From message is left blank, the system email address will be used.</returns>
        Task SendAsync(EmailMessage email);

        Task SendAsync(MimeMessage message);

        /// <summary>
        /// Sends an administrative message to the registered adminstrator email address for this application
        /// </summary>
        Task SendAdminAsync(string subject, MimeEntity body);
    }
}
