using System.Threading.Tasks;

namespace AAE.GGTool.Services
{
    public interface IGroupDefaultsService
    {
        /// <summary>
        /// Updates existing group defaults
        /// </summary>
        Task UpdateAsync(GroupDefault defaults);

        /// <summary>
        /// Gets group defaults for a specific id
        /// </summary>
        Task<GroupDefault> GetAsync(int id);
    }
}
