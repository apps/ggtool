using AAE.GGTool.Repositories;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Services;

public class GroupMemberService : IGroupMemberService
{
    private readonly IGroupMemberRepository _groupMemberRepository;
    private readonly IGroupRepository _groupRepository;

    public GroupMemberService(IGroupMemberRepository groupMemberRepository, IGroupRepository groupRepository)
    {
        _groupMemberRepository = groupMemberRepository ?? throw new ArgumentNullException(nameof(groupMemberRepository));
        _groupRepository = groupRepository ?? throw new ArgumentNullException(nameof(groupRepository));
    }

    public async Task<IList<Member>> AddAsync(string groupEmail, IList<Member> members, CancellationToken cancellationToken = default)
    {
        var existingMembers = await GetAllAsync(groupEmail, true).ConfigureAwait(false);
        var compareResult = existingMembers.Compare(members);

        var tasks = new List<Task>();

        // add new members
        var addCount = compareResult.MembersToAdd.Count;
        for (var i = 0; i < addCount; i++)
        {
            tasks.Add(_groupMemberRepository.AddMemberAsync(groupEmail, compareResult.MembersToAdd[i]));
        }

        await Task.WhenAll(tasks);

        return await GetAllAsync(groupEmail, cancellationToken: cancellationToken);
    }

    public async Task<IList<Member>> AddOrUpdateAllAsync(string groupEmail, IList<Member> members, CancellationToken cancellationToken = default)
    {
        var existingMembers = await GetAllAsync(groupEmail,true).ConfigureAwait(false);
        var compareResult = existingMembers.Compare(members);

        var tasks = new List<Task>();

        // add new members
        var addCount = compareResult.MembersToAdd.Count;
        for (var i = 0; i < addCount; i++)
        {
            tasks.Add(_groupMemberRepository.AddMemberAsync(groupEmail, compareResult.MembersToAdd[i]));
        }

        // update existing members
        var updateCount = compareResult.MembersToUpdate.Count;
        for (var i=0;i < updateCount; i++)
        {
            tasks.Add(_groupMemberRepository.UpdateMemberAsync(groupEmail, compareResult.MembersToUpdate[i].Item2));
        }

        await Task.WhenAll(tasks);

        return await GetAllAsync(groupEmail, cancellationToken: cancellationToken);
    }

    public async Task<Member> AddOrUpdateAsync(string groupEmail, Member member, CancellationToken cancellationToken = default)
    {
        var existingMember = await GetAsync(groupEmail, member.Email, true).ConfigureAwait(false);
        if (existingMember == null)
            return await _groupMemberRepository.AddMemberAsync(groupEmail, member).ConfigureAwait(false);

        // it exists - does it need to be updated?
        if (existingMember != member)
            return await _groupMemberRepository.UpdateMemberAsync(groupEmail, member).ConfigureAwait(false);

        return existingMember;
    }

    public async Task DeleteAsync(string groupEmail, string memberEmail, CancellationToken cancellationToken = default)
    {
        var existingMember = await GetAsync(groupEmail, memberEmail).ConfigureAwait(false);
        if (existingMember != null)
            await _groupMemberRepository.DeleteMemberAsync(groupEmail, memberEmail).ConfigureAwait(false);
    }

    public Task<IList<Member>> GetAllAsync(string groupEmail, bool bypassCache = false, bool includedNestedMembers = false, CancellationToken cancellationToken = default)
    {
        return _groupMemberRepository.GetAllMembersAsync(groupEmail, bypassCache, includedNestedMembers);
    }

    public Task<Member> GetAsync(string groupEmail, string memberEmail, bool bypassCache = false, CancellationToken cancellationToken = default)
    {
        return _groupMemberRepository.GetMemberAsync(groupEmail, memberEmail, bypassCache);
    }

    public async Task<IList<Member>> ReplaceAllAsync(string groupEmail, IList<Member> members, CancellationToken cancellationToken = default)
    {
        var existingMembers = await GetAllAsync(groupEmail, true).ConfigureAwait(false);
        var compareResult = existingMembers.Compare(members);

        var tasks = new List<Task>();

        // add new members
        var addCount = compareResult.MembersToAdd.Count;
        for (var i = 0; i < addCount; i++)
        {
            tasks.Add(_groupMemberRepository.AddMemberAsync(groupEmail, compareResult.MembersToAdd[i]));
        }

        // update existing members
        var updateCount = compareResult.MembersToUpdate.Count;
        for (var i = 0; i < updateCount; i++)
        {
            tasks.Add(_groupMemberRepository.UpdateMemberAsync(groupEmail, compareResult.MembersToUpdate[i].Item2));
        }

        // delete "extra" members
        var deleteCount = compareResult.ExtraMembers.Count;
        for (var i = 0; i < deleteCount; i++)
        {
            tasks.Add(_groupMemberRepository.DeleteMemberAsync(groupEmail, compareResult.ExtraMembers[i].Email));
        }

        await Task.WhenAll(tasks);

        return await GetAllAsync(groupEmail, cancellationToken: cancellationToken);
    }

    public async Task<IList<GroupMemberWithGroup>> SearchAllAsync(string search, bool bypassCache = false, CancellationToken cancellationToken = default)
    {
        // first get all the groups
        var groups = await _groupRepository.ListAsync().ConfigureAwait(false);

        // now get all the group members
        var memberTasks = new Dictionary<string, Task<IList<Member>>>();
        string group;
        for (var i = 0; i < groups.Count; i++)
        {
            group = groups[i];
            memberTasks.Add(group, _groupMemberRepository.GetAllMembersAsync(group, bypassCache));
        }

        // execute the GetMembers
        var parallelOptions = new ParallelOptions()
        {
            MaxDegreeOfParallelism = 10     // taken from WaMS library
        };

        await Parallel.ForEachAsync(memberTasks.Values, parallelOptions, async (task, cancellationToken) =>
        {
            await task;
        });

        var collectionToSearch = new List<GroupMemberWithGroup>();

        // loop through the results to get the entire collection
        Member member;
        IList<Member> members;
        foreach (KeyValuePair<string, Task<IList<Member>>> task in memberTasks)
        {
            members = task.Value.Result;
            for (var i = 0; i < members.Count; i++)
            {
                member = members[i];
                collectionToSearch.Add(new GroupMemberWithGroup(task.Key, member));
            }
        }

        // now perform the search
        var searchLower = search.ToLower();
        return collectionToSearch.Where(e => e.Email.ToLower().Contains(searchLower)).ToList();
    }
}
