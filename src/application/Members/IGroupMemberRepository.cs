using System.Collections.Generic;
using System.Threading.Tasks;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Repositories;

public interface IGroupMemberRepository
{
    /// <summary>
    /// Adds a group member to the specified group
    /// </summary>
    /// <param name="groupEmail">Group email address in the form group@domain</param>
    /// <param name="member">Member to add to the group</param>
    Task<Member> AddMemberAsync(string groupEmail, Member member);

    /// <summary>
    /// Updates an existing member in the specified group
    /// </summary>
    /// <param name="groupEmail">Group email address in the form group@domain</param>
    /// <param name="member">Member to update in the group</param>
    Task<Member> UpdateMemberAsync(string groupEmail, Member member);

    /// <summary>
    /// Deletes an existing member in the specified group
    /// </summary>
    /// <param name="groupEmail">Group email address in the form group@domain</param>
    /// <param name="memberEmail">Member email address to delete</param>
    /// <returns></returns>
    Task DeleteMemberAsync(string groupEmail, string memberEmail);

    /// <summary>
    /// Returns a single member of the specified group
    /// </summary>
    /// <param name="groupEmail">Group email address in the form group@domain</param>
    /// /// <param name="memberEmail">Member email address to get</param>
    /// <param name="bypassCache">Specifies rather to bypass the cache and to force a new query from Google</param>
    Task<Member> GetMemberAsync(string groupEmail, string memberEmail, bool bypassCache = false);

    /// <summary>
    /// Returns all members of the specified group
    /// </summary>
    /// <param name="groupEmail">Group email address in the form group@domain</param>
    /// <param name="bypassCache">Specifies rather to bypass the cache and to force a new query from Google</param>
    Task<IList<Member>> GetAllMembersAsync(string groupEmail, bool bypassCache = false, bool includedNestedMembers = false);

    /// <summary>
    /// returns all members from the group, including nest members
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <remarks>Bypasses the cache</remarks>
    /// <returns></returns>
    Task<IList<Member>> GetAllNestedMembers(string groupEmail);
}
