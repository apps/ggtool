using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Services;

public interface IGroupMemberService
{
    /// <summary>
    /// Adds new members in the specified group.  Does not update existing members and does NOT delete extra members.
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <param name="members"></param>
    /// <param name="cancellationToken"></param>
    Task<IList<Member>> AddAsync(string groupEmail, IList<Member> members, CancellationToken cancellationToken = default);

    /// <summary>
    /// Adds new or updates existing members in the specified group.  Does NOT delete extra members.
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <param name="members"></param>
    /// <param name="cancellationToken"></param>
    Task<IList<Member>> AddOrUpdateAllAsync(string groupEmail, IList<Member> members, CancellationToken cancellationToken = default);

    /// <summary>
    /// Replaces the existing group member list in the specified group with the included members.  Extra members WILL BE DELETED.
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <param name="members"></param>
    /// <param name="cancellationToken"></param>
    Task<IList<Member>> ReplaceAllAsync(string groupEmail, IList<Member> members, CancellationToken cancellationToken = default);

    /// <summary>
    /// Adds a new member or updates an existing member of the specified group
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <param name="member"></param>
    /// <param name="cancellationToken"></param>
    Task<Member> AddOrUpdateAsync(string groupEmail, Member member, CancellationToken cancellationToken = default);

    /// <summary>
    /// Deletes the specified member from the specified group
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <param name="memberEmail"></param>
    /// <param name="cancellationToken"></param>
    Task DeleteAsync(string groupEmail, string memberEmail, CancellationToken cancellationToken = default);

    /// <summary>
    /// Gets the specified member from the specified group
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <param name="memberEmail"></param>
    /// <param name="cancellationToken"></param>
    /// <param name="bypassCache"></param>
    Task<Member> GetAsync(string groupEmail, string memberEmail, bool bypassCache = false, CancellationToken cancellationToken = default);

    /// <summary>
    /// Gets all members of the specified group
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <param name="cancellationToken"></param>
    /// <param name="bypassCache"></param>
    /// <param name="includedNestedMembers"></param>
    Task<IList<Member>> GetAllAsync(string groupEmail, bool bypassCache = false, bool includedNestedMembers = false, CancellationToken cancellationToken = default);

    /// <summary>
    /// Searches all members of all groups for the specific search term
    /// </summary>
    /// <param name="search"></param>
    /// <param name="bypassCache"></param>
    /// <param name="cancellationToken"></param>
    Task<IList<GroupMemberWithGroup>> SearchAllAsync(string search, bool bypassCache = false, CancellationToken cancellationToken = default);
}
