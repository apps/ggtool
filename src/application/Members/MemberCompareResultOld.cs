using System.Collections.Generic;

namespace AAE.GGTool
{
    /// <summary>
    /// Result from doing a group member compare
    /// </summary>
    public class MemberCompareResultOld
    {

        public MemberCompareResultOld(IList<GroupMember> membersToAdd, IList<(GroupMember, GroupMember)> membersToUpdate, IList<GroupMember> extraMembers)
        {
            MembersToAdd = membersToAdd;
            ExtraMembers = extraMembers;
            MembersToUpdate = membersToUpdate;
        }

        public IList<GroupMember> MembersToAdd { get; }
        public IList<GroupMember> ExtraMembers { get; }
        public IList<(GroupMember,GroupMember)> MembersToUpdate { get; }

    }
}
