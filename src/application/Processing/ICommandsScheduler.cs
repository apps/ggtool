﻿using AAE.GGTool.Application.Configuration.Commands;
using System.Threading.Tasks;

namespace AAE.GGTool.Application.Configuration.Processing
{
    public interface ICommandsScheduler
    {
        Task EnqueueAsync<T>(ICommand<T> command);
    }
}
