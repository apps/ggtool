using AutoMapper;

namespace AAE.GGTool.Application.Profiles;

/// <summary>
/// Automapper profile for <see cref="GroupDefaultSettings"/>
/// </summary>
public class GroupDefaultSettingsProfile : Profile
{
    public GroupDefaultSettingsProfile()
    {
        CreateMap<GroupDefaultSettings, Group>();
    }
}
