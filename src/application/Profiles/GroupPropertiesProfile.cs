using AutoMapper;

namespace AAE.GGTool.Google.Profiles;

public class GroupPropertiesProfile : Profile
{
    public GroupPropertiesProfile()
    {
        CreateMap<Group, Uw.GoogleGroups.Client.Group>();
        CreateMap<Uw.GoogleGroups.Client.Group, Group>();
    }
}
