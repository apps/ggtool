using AutoMapper;

namespace AAE.GGTool.Google.Profiles;

public class GroupSettingsProfile : Profile
{
    public GroupSettingsProfile()
    {
        CreateMap<Group, Uw.GoogleGroups.Client.GroupSettings>()
            .ForMember(dest => dest.IncludeCustomFooter,
                opt => opt.MapFrom(src => src.IncludeCustomFooter.ToString().ToLower()))
            .ForMember(dest => dest.AllowWebPosting,
                opt => opt.MapFrom(src => src.AllowWebPosting.ToString().ToLower()))
            .ForMember(dest => dest.IsArchived,
                opt => opt.MapFrom(src => src.IsArchived.ToString().ToLower()))
            .ForMember(dest => dest.IncludeInGlobalAddressList,
                opt => opt.MapFrom(src => src.IncludeInGlobalAddressList.ToString().ToLower()))
            .ForMember(dest => dest.AllowExternalMembers,
                opt => opt.MapFrom(src => src.AllowExternalMembers.ToString().ToLower()));

        CreateMap<Uw.GoogleGroups.Client.GroupSettings, Group>()
            .ForMember(dest => dest.IncludeCustomFooter,
                opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.IncludeCustomFooter) && src.IncludeCustomFooter == "true"))
            .ForMember(dest => dest.AllowWebPosting,
                opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.AllowWebPosting) && src.AllowWebPosting == "true"))
            .ForMember(dest => dest.IsArchived,
                opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.IsArchived) && src.IsArchived == "true"))
            .ForMember(dest => dest.IncludeInGlobalAddressList,
                opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.IncludeInGlobalAddressList) && src.IncludeInGlobalAddressList == "true"))
            .ForMember(dest => dest.AllowExternalMembers,
                opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.AllowExternalMembers) && src.AllowExternalMembers == "true"))
            // description often lags between changes coming from Google - GroupProperties should be the authority
            .ForMember(dest => dest.Description,
                opt => opt.Ignore())
            .ForMember(dest => dest.Email,
                opt => opt.Ignore());
    }
}
