using AAE.GGTool.Application.Configuration.Commands;
using AAE.GGTool.Application.Groups;
using AAE.GGTool.Import;
using AAE.GGTool.Services;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace AAE.GGTool
{
    public static class ServiceRegistration
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(thisAssembly));

            services.AddTransient<IImportFileProcessor, ImportFileProcessor>();

            services.AddTransient<IGroupDefaultsService, GroupDefaultsService>();
            services.AddTransient<IGroupMemberService, GroupMemberService>();
            services.AddTransient<IGroupService, GroupService>();

            services.AddTransient<IGroupEditAuthorizationManager, GroupEditAuthorizationManager>();

            // add all profiles for the Application library
            services.AddAutoMapper(cfg =>
            {
                cfg.AddMaps(typeof(CommandBase));
                cfg.DisableConstructorMapping();
            });

            // needed for ExcelDataReader
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            // add all IRequestHandlers in the Application Project to DI container
            services.Scan(scan =>
                  scan.FromAssemblies(thisAssembly)
                    .AddClasses(classes =>
                    classes.AssignableTo(typeof(IRequestHandler<,>)))
                        .AsImplementedInterfaces()
                        .WithTransientLifetime());

            return services;
        }
    }
}
