@if exist "%ProgramFiles%\Microsoft Visual Studio\2022\Professional\MSBuild\Current\Bin" set PATH=%ProgramFiles%\Microsoft Visual Studio\2022\Professional\MSBuild\Current\Bin;%PATH%
@if exist "%ProgramFiles(x86)%\NuGet" set PATH=%ProgramFiles(x86)%\NuGet;%PATH%
echo Removing old NuGet packages...........
del /f /q bin\Release\*.nupkg
msbuild UW.GoogleGroups.Client.csproj /t:Build,pack /p:Configuration="Release"
@echo off

for %%A in (bin\Release\*.nupkg) do (
	
	nuget push %%A -source https://nuget.aae.wisc.edu
)
pause
