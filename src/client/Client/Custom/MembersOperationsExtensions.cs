﻿using Microsoft.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UW.GoogleGroups.Client.Models;

namespace UW.GoogleGroups.Client
{
    public static partial class MembersOperationsExtensions
    {
        /// <summary>
        /// Retrieve all pages of group members
        /// </summary>
        /// <remarks>
        /// Google's documentation:
        /// https://developers.google.com/admin-sdk/directory/v1/guides/manage-group-members#get_all_members
        /// </remarks>
        /// <param name='groupEmail'>
        /// Identifies the group in the API request. The value must be the
        /// group's email address.
        /// </param>
        /// <param name='includeDerivedMembership'>
        /// Whether to list indirect memberships
        /// </param>
        /// <param name='customHeaders'>
        /// The headers that will be added to request.
        /// </param>
        /// <param name='cancellationToken'>
        /// The cancellation token.
        /// </param>
        /// <exception cref="Microsoft.Rest.HttpOperationException">
        /// Thrown when the operation returned an invalid status code
        /// </exception>
        /// <exception cref="Microsoft.Rest.SerializationException">
        /// Thrown when unable to deserialize the response
        /// </exception>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown when a required parameter is null
        /// </exception>
        public static async Task<IList<Member>> GetAllPagesAsync(this IMembersOperations operations, string groupEmail, string includeDerivedMembership = null, Dictionary<string, List<string>> customHeaders = null, CancellationToken cancellationToken = default)
        {
            var noMorePages = false;
            var pageCount = 1;  // for logging purposes only
            var pageToken = string.Empty;

            var members = new List<Member>();

            HttpOperationResponse<Members> result;
            while (!noMorePages && pageToken != null)
            {
                result = await operations.GetAllWithHttpMessagesAsync(groupEmail, pageToken, includeDerivedMembership: includeDerivedMembership, customHeaders: customHeaders, 
                    cancellationToken: cancellationToken).ConfigureAwait(false);

                if (!result.Response.IsSuccessStatusCode)
                {
                    var responseContent = await result.Response.Content.ReadAsStringAsync(cancellationToken).ConfigureAwait(false);
                    var errorMessage = $"Error retrieving page {pageCount} of {groupEmail} member list: {result.Response.StatusCode}: {responseContent}";
                    throw new Exception(errorMessage);
                }

                if (result.Body.MembersProperty == null)
                {
                    return new List<Member>();
                }

                members.AddRange(result.Body.MembersProperty);

                pageToken = result.Body.NextPageToken;
                noMorePages = string.IsNullOrEmpty(pageToken);

                pageCount++;
            }

            return members.OrderBy(o => o.Email).ToList();
        }
    }
}
