﻿using System.Collections.Generic;
using UW.GoogleGroups.Client.Models;

namespace UW.GoogleGroups.Client
{
    /// <summary>
    /// result from doing a compare of 2 lists of member groups
    /// </summary>
    public class MemberCompareResult
    {
        public MemberCompareResult(IList<Member> membersToAdd, IList<(Member, Member)> membersInBoth, IList<Member> extraMembers)
        {
            MembersToAdd = membersToAdd;
            ExtraMembers = extraMembers;
            MembersToUpdate = membersInBoth;
        }

        public IList<Member> MembersToAdd { get; }
        public IList<Member> ExtraMembers { get; }
        public IList<(Member, Member)> MembersToUpdate { get; }
    }
}
