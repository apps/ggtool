using Microsoft.Rest;
using System;
using System.Net.Http;

namespace UW.GoogleGroups.Client
{
    public partial class UwGGroupsClient
    {
        // disposeHttpClient can be set to true, HttpClientFactory sets disposeHandler to false so that the HttpClient does not dispose the important HttpClientHandle...
        public UwGGroupsClient(Uri baseUri, HttpClient httpClient, ServiceClientCredentials credentials)
            : this(httpClient, disposeHttpClient: true)
        {
            BaseUri = baseUri ?? throw new ArgumentNullException(nameof(baseUri));
            Credentials = credentials ?? throw new ArgumentNullException(nameof(credentials));
        }

        public UwGGroupsClient(HttpClient httpClient, ServiceClientCredentials credentials)
            : this(httpClient, disposeHttpClient: true)
        {
            Credentials = credentials ?? throw new ArgumentNullException(nameof(credentials));
        }


    }
}
