﻿using System;
using System.Collections.Generic;
using System.Linq;
using UW.GoogleGroups.Client.Models;

namespace UW.GoogleGroups.Client
{
    public static class MemberListExtensions
    {
        public static MemberCompareResult Compare(this IList<Member> members, IList<Member> toCompare)
        {
            return Compare(members, toCompare, CompareMembers);
        }

        public static MemberCompareResult CompareWithoutDeliverySettings(this IList<Member> members, IList<Member> toCompare)
        {
            return Compare(members, toCompare, CompareMembersWithoutDeliverySettings);
        }
        public static MemberCompareResult Compare(this IList<Member> members, IList<Member> toCompare, Func<Member, Member, bool> compareMethod)
        {
            // creates sorted lists to hold the 3 result sets
            var addMembers = new SortedList<string, Member>();
            var updateMembers = new SortedList<string, (Member, Member)>();
            var extraMembers = new SortedList<string, Member>();

            // create a lookup of the original list
            var originalLookup = members.ToLookup(m => m.Email);

            // create a list to hold processed emails.  We will use this later to compare to see "who is left"
            // after looping through the original member list.  Those members will be marked as deleted
            var processedMembers = new Dictionary<string, string>();

            // loop through ToCompare list
            var toCompareCount = toCompare.Count;
            for (var i = 0; i < toCompareCount; i++)
            {
                var toCompareMember = toCompare[i];

                // try and match a member in the original list
                if (originalLookup.Contains(toCompareMember.Email))
                {
                    var originalMember = originalLookup[toCompareMember.Email].First();

                    if (!compareMethod(toCompareMember, originalMember))
                        updateMembers.TryAdd(toCompareMember.Email, (originalMember, toCompareMember));

                    // add the comparedMember to processedMembers
                    processedMembers.TryAdd(originalMember.Email, originalMember.Email);

                    // member was found, we're done with this loop
                    continue;

                }

                // if we've made it this far, the member doesn't exist.  It's a "new" member
                addMembers.TryAdd(toCompareMember.Email, toCompareMember);
            }

            // now compare the list of processed members (which is the list of toCompare members that were found in the original member list) 
            // to the original list to get who is "left over".  These members would be deleted (or are extras)

            var memberCount = members.Count;
            for (var i = 0; i < memberCount; i++)
            {
                var originalMember = members[i];
                if (!processedMembers.ContainsKey(originalMember.Email))
                    extraMembers.TryAdd(originalMember.Email, originalMember);
            }

            return new MemberCompareResult(membersToAdd: addMembers.Values, extraMembers: extraMembers.Values, membersInBoth: updateMembers.Values);
        }

        public static bool CompareMembers(Member member1, Member member2)
        {
            return CompareMembersWithoutDeliverySettings(member1, member2) &&
                    member1.DeliverySettings == member2.DeliverySettings;
        }
        public static bool CompareMembersWithoutDeliverySettings(Member member1, Member member2)
        {
            return member2 != null &&
                    (member1.Equals(member2) ||
                    (member1.Email.ToLower() == member2.Email.ToLower() &&
                    member1.Role == member2.Role));
        }
    }
}
