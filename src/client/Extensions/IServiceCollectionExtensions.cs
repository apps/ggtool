using AAE.Api;
using AAE.Api.Authentication;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Rest;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Net.Http;
using UW.GoogleGroups.Client;

namespace UW.GoogleGroups
{
    public static class IServiceCollectionExtensions
    {
        /// <summary>
        /// Initializes the API service client with token provider using <see cref="IdentityAuthorityConfig"/> and IHttpClientFactory.  Uses the default BaseUri.
        /// </summary>
        /// <param name="identityConfig">Configuration used to identity the app utilizing this API service client</param>
        /// <param name="services"></param>
        public static IServiceCollection AddUwGoogleGroupsClient(this IServiceCollection services, IdentityAuthorityConfig identityConfig)
        {

            return services.AddUwGoogleGroupsClient(identityConfig, null);
        }

        /// <summary>
        /// Initializes the API Service client with token provider and IHttpClientFactory  Specifies a <see cref="ServiceClientConfig"/> to override <see cref="HttpClient"/> configurations.
        /// </summary>
        /// <param name="identityConfig">Configuration used to identity the app utilizing this API service client</param>
        /// <param name="clientConfig">API Service Client configuration options</param>
        /// <param name="services"></param>
        public static IServiceCollection AddUwGoogleGroupsClient(this IServiceCollection services, IdentityAuthorityConfig identityConfig, ServiceClientConfig clientConfig)
        {

            // injecting IHttpClientFactory and a named HttpClient
            services.AddHttpClient<IUwGGroupsClient, UwGGroupsClient>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(1))    // lifetime is 1 minute
                .AddPolicyHandler(GetRetryPolicy());

            services.AddTransient<IUwGGroupsClient, UwGGroupsClient>(sp =>
            {

                // get the HttpClientFactory
                var httpClientFactory = sp.GetRequiredService<IHttpClientFactory>();

                // create a named/configured HttpClient
                var httpClient = httpClientFactory.CreateClient(nameof(IUwGGroupsClient));

                var cache = sp.GetRequiredService<IMemoryCache>();

                var tokenProvider = new OAuthTokenProvider(identityConfig, httpClientFactory, cache);

                // see if overriding config was specified
                if (clientConfig != null && !string.IsNullOrEmpty(clientConfig.BaseUrl))
                {
                    // get the base Uri from configuration
                    var baseUri = new Uri(clientConfig.BaseUrl);
                    return new UwGGroupsClient(baseUri, httpClient, new TokenCredentials(tokenProvider));
                }
                else
                {
                    // use the default base uri
                    return new UwGGroupsClient(httpClient, new TokenCredentials(tokenProvider));
                }
            });



            return services;
        }

        /// <summary>
        /// Retry policy to 502 BadGateway errors 3 times
        /// </summary>
        /// <returns></returns>
        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.BadGateway)
                .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromMilliseconds(500));
        }
    }
}
