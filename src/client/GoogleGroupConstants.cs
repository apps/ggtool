using System;
using System.Collections.Generic;
using System.Text;

namespace UW.GoogleGroups.Client
{
    public static class GoogleGroupConstants
    {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>https://developers.google.com/admin-sdk/directory/v1/reference/members</remarks>
        public static class DeliverySettings
        {

            public const string NoEmail = "NONE";
            public const string AllEmail = "ALL_MAIL";
            public const string Daily = "DAILY";
            public const string Digest = "DIGEST";
            public const string Disabled = "DISABLED";
        }


        public static class Role
        {
            public const string Owner = "OWNER";
            public const string Member = "MEMBER";
            public const string Manager = "MANAGER";
        }

        public static class WhoCanLeave
        {
            public const string Managers = "ALL_MANAGERS_CAN_LEAVE";
            public const string Members = "ALL_MEMBERS_CAN_LEAVE";
            public const string None = "NONE_CAN_LEAVE";
        }

        public static class WhoCanViewMembership
        {
            public const string AllInDomain = "ALL_IN_DOMAIN_CAN_VIEW";
            public const string AllMembers = "ALL_MEMBERS_CAN_VIEW";
            public const string AllManagers = "ALL_MANAGERS_CAN_VIEW";
        }

        public static class WhoCanJoin
        {
            public const string Anyone = "ANYONE_CAN_JOIN";
            public const string AllInDomain = "ALL_IN_DOMAIN_CAN_JOIN";
            public const string Invited = "INVITED_CAN_JOIN";
            public const string CanRequest = "CAN_REQUEST_TO_JOIN";
        }

        public static class WhoCanPost
        {
            public const string None = "NONE_CAN_POST";
            public const string ManagersAndOwners = "ALL_MANAGERS_CAN_POST";
            public const string GroupMembers = "ALL_MEMBERS_CAN_POST";
            public const string OnlyOwners = "ALL_OWNERS_CAN_POST";
            public const string AllInDomain = "ALL_IN_DOMAIN_CAN_POST";
            public const string Anyone = "ANYONE_CAN_POST";
        }
    }
}
