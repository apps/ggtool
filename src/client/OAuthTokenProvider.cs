﻿using IdentityModel.Client;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Rest;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace AAE.Api.Authentication
{
    public class OAuthTokenProvider : ITokenProvider
    {
        private readonly IIdentityAuthorityConfig _identityConfig;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IMemoryCache _cache;

        private DateTimeOffset _expiration;
        private string _accessToken;
        private string _accessTokenType;
        private static readonly TimeSpan ExpirationThreshold = TimeSpan.FromMinutes(5);

        public OAuthTokenProvider(IIdentityAuthorityConfig identityConfig, IHttpClientFactory httpClientFactory, IMemoryCache cache)
        {
            _identityConfig = identityConfig ?? throw new ArgumentNullException(nameof(identityConfig));
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));

            Initialize();
        }

        public async Task<AuthenticationHeaderValue> GetAuthenticationHeaderAsync(CancellationToken cancellationToken)
        {
            await PrepAccessTokenForUse(cancellationToken);
                

            return new AuthenticationHeaderValue(_accessTokenType, _accessToken);
        }

        /// <summary>
        /// Sends out an initial token access request if the cached once doesn't exist or is expired.
        /// </summary>
        protected void Initialize()
        {
            LoadFromCache();
            PrepAccessTokenForUse().GetAwaiter().GetResult();

        }

        private async Task PrepAccessTokenForUse(CancellationToken cancellationToken = default(CancellationToken))
        {
            if (AccessTokenExpired)
            {
                var client = _httpClientFactory.CreateClient();

                var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest()
                {
                    Address = _identityConfig.AuthorityUrl,

                    ClientId = _identityConfig.ClientId,
                    ClientSecret = _identityConfig.ClientSecret,
                    Scope = _identityConfig.Scope
                }, cancellationToken);

                if (tokenResponse.IsError)
                {
                    throw new Exception(tokenResponse.Error);
                }

                SetAccessTokenCache(tokenResponse);
            }
        }

        protected virtual bool AccessTokenExpired
        {
            get { return DateTime.UtcNow + ExpirationThreshold >= _expiration; }
        }

        private void LoadFromCache()
        {
            if (_cache.TryGetValue(CacheKey(), out TokenResponse tokenResponse))
                LoadVariables(tokenResponse);
        }

        private void LoadVariables(TokenResponse tokenResponse)
        {
            _accessToken = tokenResponse.AccessToken;
            _accessTokenType = tokenResponse.TokenType;
            _expiration = tokenResponse.HttpResponse.Headers.Date.Value.Add(TimeSpan.FromSeconds(tokenResponse.ExpiresIn));
        }

        private string CacheKey()
        {
            return _identityConfig.ClientId;
        }

        private void SetAccessTokenCache(TokenResponse tokenResponse)
        {
            // load the variables so they are available for use
            LoadVariables(tokenResponse);

            // remove the token from the cache after it expires
            var options = new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromSeconds(tokenResponse.ExpiresIn));

            _cache.Set(CacheKey(), tokenResponse, options);
        }

    }
}
