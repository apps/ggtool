@echo off
echo Deleting previously generated files
del /s /q .\Client\Generated
echo Generating new client
autorest autorest.yaml --version=3.0.6320