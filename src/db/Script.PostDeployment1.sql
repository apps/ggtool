﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
SET IDENTITY_INSERT [dbo].[group_default] ON 

INSERT [dbo].[group_default] ([id], [settings]) VALUES (1001, N'{"AllowExternalMembers":true,"AllowWebPosting":false,"CustomFooterText":"","IsArchived":true,"IncludeCustomFooter":false,"IncludeInGlobalAddressList":false,"MessageModerationLevel":"MODERATE_NONE","SpamModerationLevel":"ALLOW","WhoCanJoin":"INVITED_CAN_JOIN","WhoCanLeaveGroup":"NONE_CAN_LEAVE","WhoCanViewGroup":"ALL_MEMBERS_CAN_VIEW","WhoCanViewMembership":"ALL_MANAGERS_CAN_VIEW","WhoCanPostMessage":"ALL_MEMBERS_CAN_POST"}')
SET IDENTITY_INSERT [dbo].[group_default] OFF
GO
