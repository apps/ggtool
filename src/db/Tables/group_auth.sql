﻿CREATE TABLE [dbo].[group_auth]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1001, 1), 
    [netid] VARCHAR(50) NOT NULL, 
    [group] NCHAR(10) NOT NULL, 
    [uw_app_id] INT NOT NULL, 
    [add_time] DATETIME2(0) NOT NULL DEFAULT sysutcdatetime(), 
    CONSTRAINT [FK_group_auth_uw_app] FOREIGN KEY ([uw_app_id]) REFERENCES [uw_app]([id]) ON DELETE CASCADE
)

GO


CREATE UNIQUE INDEX [UQ_group_auth_netid_group_uwapp] ON [dbo].[group_auth] ([netid], [group], [uw_app_id])
