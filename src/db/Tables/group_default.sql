﻿CREATE TABLE [dbo].[group_default]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1001, 1), 
    [settings] NVARCHAR(MAX) NULL, 
    [add_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime(), 
    [update_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime(), 
    
)

GO
