﻿CREATE TABLE [dbo].[internal_command]
(
	[id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [enqueue_date] DATETIME2(0) NOT NULL DEFAULT sysutcdatetime(), 
    [type] VARCHAR(255) NOT NULL, 
    [data] NVARCHAR(MAX) NOT NULL, 
    [processed_date] DATETIME2(0) NULL
)
