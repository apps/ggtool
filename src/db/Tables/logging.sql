﻿CREATE TABLE [dbo].[logging]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Level] [nvarchar](max) NULL,
	[Timestamp] [datetime] NULL,
	[Exception] [nvarchar](max) NULL,
	[SourceContext] [varchar](250) NULL, 
    CONSTRAINT [PK_logging] PRIMARY KEY ([Id]),
)
