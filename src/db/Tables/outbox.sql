﻿CREATE TABLE [dbo].[outbox]
(
	[id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [occurred_on] DATETIME2(0) NOT NULL, 
    [processed_date] DATETIME2(0) NULL, 
    [type] VARCHAR(255) NULL, 
    [data] NVARCHAR(MAX) NULL
)
