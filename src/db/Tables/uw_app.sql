﻿CREATE TABLE [dbo].[uw_app]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [app_id] VARCHAR(255) NOT NULL, 
    [add_time] DATETIME2(0) NOT NULL DEFAULT sysutcdatetime()
)
