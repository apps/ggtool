using System;

namespace AAE.GGTool
{
    /// <summary>
    /// Defaults for group settings
    /// </summary>
    public class GroupDefault
    {
        public int Id { get; set; } = default;
        public DateTime LastUpdated { get; set; }
        public GroupDefaultSettings Defaults { get; set; } = new GroupDefaultSettings();
    }
}
