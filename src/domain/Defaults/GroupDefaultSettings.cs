namespace AAE.GGTool
{
    public class GroupDefaultSettings
    {
        /// <summary>
        /// </summary>
        public bool AllowExternalMembers { get; set; }

        /// <summary>
        /// </summary>
        public bool IncludeInGlobalAddressList { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanJoin { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanViewMembership { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanViewGroup { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanPostMessage { get; set; }

        /// <summary>
        /// </summary>
        public bool AllowWebPosting { get; set; }

        /// <summary>
        /// </summary>
        public string MessageModerationLevel { get; set; }

        /// <summary>
        /// </summary>
        public string SpamModerationLevel { get; set; }

        /// <summary>
        /// </summary>
        public bool IncludeCustomFooter { get; set; }

        /// <summary>
        /// </summary>
        public string CustomFooterText { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanLeaveGroup { get; set; }

        /// <summary>
        /// </summary>
        public bool IsArchived { get; set; }
    }
}
