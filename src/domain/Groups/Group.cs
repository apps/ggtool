namespace AAE.GGTool
{
    public class Group
    {
        /// <summary>
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// </summary>
        public bool AllowExternalMembers { get; set; }

        /// <summary>
        /// </summary>
        public bool IncludeInGlobalAddressList { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanJoin { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanViewMembership { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanViewGroup { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanPostMessage { get; set; }

        /// <summary>
        /// </summary>
        public bool AllowWebPosting { get; set; }

        /// <summary>
        /// </summary>
        public string MessageModerationLevel { get; set; }

        /// <summary>
        /// </summary>
        public string SpamModerationLevel { get; set; }

        /// <summary>
        /// </summary>
        public bool IncludeCustomFooter { get; set; }

        /// <summary>
        /// </summary>
        public string CustomFooterText { get; set; }

        /// <summary>
        /// </summary>
        public string WhoCanLeaveGroup { get; set; }

        /// <summary>
        /// </summary>
        public bool IsArchived { get; set; }

        public string EmailKey
        {
            get
            {
                if (Email.Contains("@"))
                    return Email.Substring(0,Email.IndexOf("@"));

                return string.Empty;
            }
        }

        public string DomainKey
        {
            get
            {
                if (Email.Contains("@"))
                    return Email.Substring(Email.IndexOf("@") + 1);

                return string.Empty;
            }
        }
    }
}
