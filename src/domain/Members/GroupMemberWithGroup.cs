using Uw.GoogleGroups.Client;

namespace AAE.GGTool;

public class GroupMemberWithGroup : Member
{
    public GroupMemberWithGroup()
    {
    }

    public GroupMemberWithGroup(string group, Member groupMember)
    {
        Group = group;
        Id = groupMember.Id;
        Role = groupMember.Role;
        Email = groupMember.Email;
        Kind = groupMember.Kind;
        DeliverySettings = groupMember.DeliverySettings;
        Type = groupMember.Type;
        Status = groupMember.Status;
    }

    public string Group { get; set; }
}
