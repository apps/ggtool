using MediatR;
using System;

namespace AAE.GGTool.Domain.SeedWork
{
    public interface IDomainEvent : INotification
    {
        DateTime OccurredOn { get; }
    }
}
