namespace AAE.GGTool.Domain.Settings
{
    /// <summary>
    /// Class to hold app email settings
    /// </summary>
    public class MailSettings
    {
        public string MailServer { get; set; }
        public int MailPort { get; set; }
        public string SenderName { get; set; }
        public string Sender { get; set; }
        public string SysAdminEmail { get; set; }
    }
}
