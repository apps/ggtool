using Azure.Core;

namespace AAE.GGTool;

public class AppInfrastructureOptions
{
    /// <summary>
    /// Connection string for GGTool database
    /// </summary>
    public string? ConnectionString { get; set; }

    public TokenCredential GoogleGroupCredential { get; set; }
}
