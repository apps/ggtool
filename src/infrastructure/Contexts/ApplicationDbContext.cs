using AAE.GGTool.Infrastructure.Processing.InternalCommands;
using AAE.GGTool.Infrastructure.Processing.Outbox;
using Microsoft.EntityFrameworkCore;

namespace AAE.GGTool.Infrastructure.Contexts
{
    // TODO - change to internal - only use repositories to talk to Context
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            //ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public DbSet<InternalCommand> InternalCommands { get; set; }
        public DbSet<OutboxMessage> OutboxMessages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }
    }
}
