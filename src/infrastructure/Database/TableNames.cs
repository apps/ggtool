namespace AAE.GGTool.Infrastructure.Database
{
    internal sealed class TableNames
    {
#pragma warning disable IDE1006 // Naming Styles - no "_" in internal const
        internal const string Users = "app_user";
        internal const string OutboxMessages = "outbox";
        internal const string InternalCommands = "internal_command";
#pragma warning restore IDE1006 // Naming Styles
    }
}
