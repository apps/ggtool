﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.GGTool.Infrastructure.Exceptions
{
    public class ConnectionClosedException : Exception
    {
        public ConnectionClosedException() : base("Open connection is needed.  Closed connection provided.")
        {
        }
    }
}
