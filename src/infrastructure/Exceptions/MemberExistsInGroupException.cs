﻿using System;

namespace AAE.GGTool.Google.Exceptions
{
    /// <summary>
    /// Exception throw when a member already exists in a group
    /// </summary>
    public class MemberExistsInGroupException : Exception
    {
        public MemberExistsInGroupException(string memberEmail, string groupEmail) :
            base(message: $"{memberEmail} already exists in {groupEmail}")
        {
            MemberEmail = memberEmail;
            GroupEmail = groupEmail;
        }

        public string MemberEmail { get; }
        public string GroupEmail { get; }
    }
}
