using System.Data;
using Microsoft.Data.SqlClient;

namespace AAE.Data
{
    /// <summary>
    /// Implementation of <see cref="DbConnectionFactoryBase"/> for SQL Server
    /// </summary>
    public class SqlConnectionFactory : DbConnectionFactoryBase
    {
        public SqlConnectionFactory(string connectionString) : base(connectionString)
        {
        }

        /// <inheritdoc />
        public override IDbDataParameter GetDbParameter()
        {
            return new SqlParameter();
        }

        /// <inheritdoc />
        public override IDbDataParameter GetDbParameter(string parameterName, DbType dbType)
        {
            var param = new SqlParameter();
            param.DbType = dbType;
            param.ParameterName = parameterName;
            return param;
        }

        /// <inheritdoc />
        public override IDbDataParameter GetDbParameter(string parameterName, object value)
        {
            return new SqlParameter(parameterName, value);
        }

        /// <inheritdoc />
        public override IDbDataParameter GetDbParameter(string parameterName, DbType dbType, int size)
        {
            var param = new SqlParameter();
            param.DbType = dbType;
            param.ParameterName = parameterName;
            param.Size = size;
            return param;
        }

        /// <inheritdoc />
        protected override IDbConnection GetConnection(string connectionString = "")
        {
            return new SqlConnection(connectionString);
        }
    }
}
