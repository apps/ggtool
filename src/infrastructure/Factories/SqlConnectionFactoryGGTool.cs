﻿using AAE.Data;
using AAE.GGTool.Application.Interfaces;

namespace AAE.GGTool.Infrastructure
{
    /// <summary>
    /// Connection factory for GGTool database
    /// </summary>
    public class SqlConnectionFactoryGGTool : SqlConnectionFactory, IDbConnectionFactoryGGTool
    {
        public SqlConnectionFactoryGGTool(string connectionString) : base(connectionString)
        {
        }
    }
}
