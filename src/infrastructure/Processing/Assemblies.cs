﻿using AAE.Data;
using AAE.GGTool.Application.Configuration.Commands;
using System.Reflection;

namespace AAE.GGTool.Infrastructure
{
    internal static class Assemblies
    {
        public static readonly Assembly Application = typeof(ICommand).Assembly;
        public static readonly Assembly Infrastructure = typeof(DbConnectionFactoryBase).Assembly;
    }
}
