﻿using AAE.GGTool.Application.Configuration.Commands;
using MediatR;
using System.Threading.Tasks;

namespace AAE.GGTool.Infrastructure.Processing
{
    internal static class CommandsExecutor
    {
        public static async Task Execute(ICommand command)
        {
            using (var scope = ServiceActivator.GetScope())
            {
                var mediator = scope.ServiceProvider.GetService(typeof(IMediator)) as IMediator;
                await mediator.Send(command).ConfigureAwait(false);
            }
        }

        public static async Task<TResult> Execute<TResult>(ICommand<TResult> command)
        {
            using (var scope = ServiceActivator.GetScope())
            {
                var mediator = scope.ServiceProvider.GetService(typeof(IMediator)) as IMediator;
                return await mediator.Send(command).ConfigureAwait(false);
            }
        }
    }
}
