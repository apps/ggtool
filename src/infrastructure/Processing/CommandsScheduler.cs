﻿using AAE.Data;
using AAE.GGTool.Application.Configuration.Commands;
using AAE.GGTool.Application.Configuration.Processing;
using AAE.GGTool.Application.Interfaces;
using AAE.GGTool.Infrastructure.Database;
using Dapper;
using System.Text.Json;
using System.Threading.Tasks;

namespace AAE.GGTool.Infrastructure.Processing
{
    public class CommandsScheduler : ICommandsScheduler
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public CommandsScheduler(IDbConnectionFactoryGGTool dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task EnqueueAsync<T>(ICommand<T> command)
        {
            using (var connection = _dbConnectionFactory.GetDbConnection())
            {
                const string sql = @$"INSERT INTO {TableNames.InternalCommands} (id, [type], data) VALUES (@id, @type, @data);";

                var type = command.GetType();

                await connection.ExecuteAsync(sql, new
                {
                    id = command.Id,
                    type = type.FullName,
                    data = JsonSerializer.Serialize(command, type)
                }).ConfigureAwait(false);
            }
        }
    }
}
