using AAE.GGTool.Application.Configuration.DomainEvents;
using AAE.GGTool.Domain.SeedWork;
using AAE.GGTool.Infrastructure.Contexts;
using AAE.GGTool.Infrastructure.Processing.Outbox;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace AAE.GGTool.Infrastructure.Processing
{
    public class DomainEventsDispatcher : IDomainEventsDispatcher
    {
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _dbContext;
        private readonly IServiceProvider _serviceProvider;
        private readonly IServiceCollection _services;

        public DomainEventsDispatcher(IMediator mediator, ApplicationDbContext dbContext, IServiceProvider serviceProvider, IServiceCollection services)
        {
            _mediator = mediator;
            _dbContext = dbContext;
            _serviceProvider = serviceProvider;
            _services = services;
        }

        public async Task DispatchEventsAsync()
        {
            // get any domain entities that have been changed (and not committed)
            var domainEntities = _dbContext.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any()).ToList();

            // get all the domain events from those entities
            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();

            // create a domain event notification for every domain event
            var domainEventNotifications = new List<IDomainEventNotification<IDomainEvent>>();
            foreach (var domainEvent in domainEvents)
            {
                Type domainEventNotificationType = typeof(IDomainEventNotification<>);
                var domainEventNotificationWithGenericType = domainEventNotificationType.MakeGenericType(domainEvent.GetType());

                var concreteType = _services.GetImplementationTypes(domainEventNotificationWithGenericType).FirstOrDefault();

                // if concreteType is null, a notification hasn't been created for this domain event
                // skip the rest of the loop if that's the case
                if (concreteType == null)
                    continue;

                var domainNotification = Activator.CreateInstance(concreteType, domainEvent);

                if (domainNotification != null)
                    domainEventNotifications.Add(domainNotification as IDomainEventNotification<IDomainEvent>);
            }

            // clear all the domain events
            domainEntities
                .ForEach(entity => entity.Entity.ClearDomainEvents());

            // fire off all domain events
            var tasks = domainEvents
                .Select(async (domainEvent) =>
                {
                    await _mediator.Publish(domainEvent);
                });

            await Task.WhenAll(tasks);

            // store all the domain event notifications in the Outbox
            foreach (var domainEventNotification in domainEventNotifications)
            {
                var type = domainEventNotification.GetType();
                var typeName = type.FullName;
                var data = JsonSerializer.Serialize(domainEventNotification, type);
                var outboxMessage = new OutboxMessage(domainEventNotification.DomainEvent.OccurredOn, typeName, data);
                _dbContext.OutboxMessages.Add(outboxMessage);
            }
        }
    }
}
