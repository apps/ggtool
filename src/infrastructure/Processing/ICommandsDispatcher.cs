﻿using System;
using System.Threading.Tasks;

namespace AAE.GGTool.Infrastructure.Processing
{
    internal interface ICommandsDispatcher
    {
        Task DispatchCommandAsync(Guid id);
    }
}
