using System.Threading.Tasks;

namespace AAE.GGTool.Infrastructure.Processing
{
    public interface IDomainEventsDispatcher
    {
        Task DispatchEventsAsync();
    }
}
