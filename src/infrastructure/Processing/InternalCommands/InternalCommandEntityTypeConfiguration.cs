﻿using AAE.GGTool.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AAE.GGTool.Infrastructure.Processing.InternalCommands
{
    internal sealed class InternalCommandEntityTypeConfiguration : IEntityTypeConfiguration<InternalCommand>
    {
        public void Configure(EntityTypeBuilder<InternalCommand> builder)
        {
            builder.ToTable(TableNames.InternalCommands);

            builder.HasKey(b => b.Id);
            builder.Property(b => b.Id).ValueGeneratedNever();

            builder.Property(e => e.Type)
                .IsUnicode(false)
                .HasMaxLength(255);

            builder.Property(e => e.Data)
                .IsUnicode(true);

            builder.Property(e => e.ProcessedDate)
                .HasColumnName("processed_date");
        }
    }
}