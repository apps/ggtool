﻿using AAE.Data;
using AAE.GGTool.Application.Configuration.Commands;
using AAE.GGTool.Application.Interfaces;
using AAE.GGTool.Infrastructure.Database;
using AAE.GGTool.Infrastructure.Processing.Outbox;
using Dapper;
using MediatR;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace AAE.GGTool.Infrastructure.Processing.InternalCommands
{
    internal class ProcessInternalCommandsCommand : CommandBase<Unit>, IRecurringCommand
    {
    }

    internal class ProcessInternalCommandsCommandHandler : ICommandHandler<ProcessInternalCommandsCommand, Unit>
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public ProcessInternalCommandsCommandHandler(IDbConnectionFactoryGGTool dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<Unit> Handle(ProcessInternalCommandsCommand command, CancellationToken cancellationToken)
        {
            using(var connection = _dbConnectionFactory.GetDbConnection())
            {
                const string sql = $"SELECT [type], data FROM {TableNames.InternalCommands} WHERE processed_date IS NULL;";

                var commands = await connection.QueryAsync<InternalCommandDto>(sql);

                var internalCommandsList = commands.AsList();
                foreach (var internalCommand in internalCommandsList)
                {
                    Type type = Assemblies.Application.GetType(internalCommand.Type);
                    dynamic commandToProcess = JsonSerializer.Deserialize(internalCommand.Data, type);

                    await CommandsExecutor.Execute(commandToProcess);
                }
            }

            return Unit.Value;
        }

        private class InternalCommandDto
        {
            public string Type { get; set; }

            public string Data { get; set; }
        }
    }
}