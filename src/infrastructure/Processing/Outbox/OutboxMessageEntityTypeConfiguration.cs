﻿using AAE.GGTool.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AAE.GGTool.Infrastructure.Processing.Outbox
{
    internal sealed class OutboxMessageEntityTypeConfiguration : IEntityTypeConfiguration<OutboxMessage>
    {
        public void Configure(EntityTypeBuilder<OutboxMessage> builder)
        {
            builder.ToTable(TableNames.OutboxMessages);

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .ValueGeneratedNever();

            builder.Property(e => e.Data)
                .IsUnicode(true);

            builder.Property(e => e.Type)
                .IsUnicode(false)
                .HasMaxLength(255);

            builder.Property(e => e.OccurredOn)
                .HasColumnName("occurred_on");

            builder.Property(e => e.ProcessedDate)
                .HasColumnName("processed_date");
        }
    }
}
