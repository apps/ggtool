﻿using AAE.Data;
using AAE.GGTool.Application.Configuration.Commands;
using AAE.GGTool.Application.Configuration.DomainEvents;
using AAE.GGTool.Application.Interfaces;
using Dapper;
using MediatR;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace AAE.GGTool.Infrastructure.Processing.Outbox
{
    internal class ProcessOutboxCommand : CommandBase<Unit>, IRecurringCommand
    {
    }

    internal class ProcessOutboxCommandHandler : ICommandHandler<ProcessOutboxCommand, Unit>
    {
        private readonly IMediator _mediator;

        private readonly IDbConnectionFactory _dbConnectionFactory;

        public ProcessOutboxCommandHandler(IMediator mediator, IDbConnectionFactoryGGTool dbConnectionFactory)
        {
            _mediator = mediator;
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<Unit> Handle(ProcessOutboxCommand command, CancellationToken cancellationToken)
        {
            using (var connection = _dbConnectionFactory.GetDbConnection())
            {
                const string sql = @"SELECT id 
                                ,occurred_on AS OccurredOn
                                ,[type]
                                ,data
                                ,processed_date AS ProcessedDate

                                FROM outbox 
                                WHERE processed_date IS NULL;";

                const string sqlUpdateProcessedDate = @"UPDATE outbox SET processed_date = @date WHERE id=@id;";

                var messages = await connection.QueryAsync<OutboxMessageDto>(sql).ConfigureAwait(false);
                var messagesList = messages.AsList();

                if (messagesList.Count > 0)
                {
                    foreach (var message in messagesList)
                    {
                        Type type = Assemblies.Application.GetType(message.Type);
                        var request = JsonSerializer.Deserialize(message.Data, type) as IDomainEventNotification;

                        await _mediator.Publish(request, cancellationToken);

                        await connection.ExecuteAsync(sqlUpdateProcessedDate, new {date = DateTime.UtcNow, id = message.Id});
                    }
                }

                return Unit.Value;
            }
        }
    }
}
