using AAE.GGTool.Application.Configuration.Commands;
using AAE.GGTool.Domain.SeedWork;
using AAE.GGTool.Infrastructure.Contexts;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AAE.GGTool.Persistence.Processing;

/// <summary>
/// Pipeline behavior to commmit a unit of work on an <see cref="ICommand"/>
/// </summary>
/// <typeparam name="TRequest"></typeparam>
/// <typeparam name="TResponse"></typeparam>
public class UnitOfWorkPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : ICommand<TResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private ApplicationDbContext _dbContext;

    public UnitOfWorkPipelineBehavior(IUnitOfWork unitOfWork, ApplicationDbContext dbContext)
    {
        _unitOfWork = unitOfWork;
        _dbContext = dbContext;
    }

    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        var response = await next();

        if (request is InternalCommandBase<TResponse>)
        {
            var internalCommand = await _dbContext.InternalCommands.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken: cancellationToken).ConfigureAwait(false);
            if (internalCommand != null)
            {
                internalCommand.ProcessedDate = DateTime.UtcNow;
            }
        }

        await _unitOfWork.CommitAsync(cancellationToken);

        return response;
    }
}
