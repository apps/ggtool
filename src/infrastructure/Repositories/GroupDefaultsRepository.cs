using AAE.Data;
using AAE.GGTool.Application.Interfaces;
using Dapper;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace AAE.GGTool.Repositories
{
    public class GroupDefaultsRepository : IGroupDefaultsRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public GroupDefaultsRepository(IDbConnectionFactoryGGTool dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory ?? throw new ArgumentNullException(nameof(dbConnectionFactory));
        }

        public async Task<GroupDefault> GetAsync(int id)
        {
            using (var connection = _dbConnectionFactory.GetDbConnection())
            {
                var sql = @"SELECT id, settings, update_time FROM group_default WHERE id=@id;";

                using (var rdr = await connection.ExecuteReaderAsync(sql, new { id }))
                {
                    var settingsIndex = rdr.GetOrdinal("settings");
                    var updateIndex = rdr.GetOrdinal("update_time");

                    GroupDefault groupDefault = null;

                    while (rdr.Read())
                    {
                        groupDefault = new GroupDefault()
                        {
                            Id = id,
                            LastUpdated = rdr.GetDateTime(updateIndex),
                            Defaults = JsonSerializer.Deserialize<GroupDefaultSettings>(rdr.GetString(settingsIndex))
                        };
                    }

                    return groupDefault;
                }
            }
        }

        public async Task UpdateAsync(GroupDefault defaults)
        {
            using (var connection = _dbConnectionFactory.GetDbConnection())
            {
                var sql = @"UPDATE group_default SET settings=@settings, update_time=sysutcdatetime() WHERE id=@id;";

                var input = new
                {
                    id = defaults.Id,
                    settings = JsonSerializer.Serialize(defaults.Defaults)
                };

                await connection.ExecuteAsync(sql, input).ConfigureAwait(false);
            }
        }
    }
}
