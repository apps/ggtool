using Aae.Caching;
using AAE.GGTool.Google.Exceptions;
using AutoMapper;
using Azure;
using Microsoft.Extensions.Logging;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Repositories;

public class GroupMemberRepository : IGroupMemberRepository
{
    private readonly MembersClient _membersClient;
    private readonly ICache<SortedList<string, Member>> _cache;
    private readonly IMapper _mapper;
    private readonly ILogger _logger;

    public GroupMemberRepository(
        UwGoogleGroupsClient groupsClient,
        ICache<SortedList<string, Member>> cache,
        IMapper mapper,
        ILogger<GroupMemberRepository> logger)
    {
        _membersClient = groupsClient?.GetMembersClient() ?? throw new ArgumentNullException(nameof(groupsClient));
        _cache = cache ?? throw new ArgumentNullException(nameof(cache));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    public async Task<Member> AddMemberAsync(string groupEmail, Member member)
    {
        var result = await _membersClient.AddAsync(groupEmail, member);
        var resultResponse = result.GetRawResponse();

        if (resultResponse.IsError)
        {
            // member already exists - possibly due to an alternative email address
            // try re-sending as an update
            if (resultResponse.Status == (int)System.Net.HttpStatusCode.Conflict)
            {
                _logger.LogError("{memberEmail} already exists in {groupEmail}", member.Email, groupEmail);

                // put this in a try-catch - if there is an error on this resend, swallow it and send the MemberExists exception
                try
                {
                    return await UpdateMemberAsync(groupEmail, member).ConfigureAwait(false);
                }
                catch
                {
                    // swallow the subsequent exception
                }

                // if it made this far, throw this original exception for not being able to add the member.
                throw new MemberExistsInGroupException(memberEmail: member.Email, groupEmail: groupEmail);
            }
            else
            {
                _logger.LogError("An error occurred while updating Google: adding {memberEmail} to {groupEmail}", member.Email, groupEmail);
                RequestErrorLogging(resultResponse);

                throw new Exception($"An error occurred while updating Google. {resultResponse.ReasonPhrase} {resultResponse.Content}");
            }
        }

        var returnedMember = result.Value;

        var group = await GetCachedGroupAsync(groupEmail).ConfigureAwait(false);
        if (group != null)
        {
            group.Add(returnedMember.Email, returnedMember);
        }

        return returnedMember;
    }

    public async Task DeleteMemberAsync(string groupEmail, string memberEmail)
    {
        var resultResponse = await _membersClient.DeleteAsync(groupEmail, memberEmail).ConfigureAwait(false);

        if (resultResponse.IsError)
        {
            //TODO: move this out of an exception to something more graceful and system-wide
            if (resultResponse.Status == (int)System.Net.HttpStatusCode.NotFound)
            {
                _logger.LogError("{memberEmail} does not exist in {groupEmail}", memberEmail, groupEmail);
                throw new Exception($"{memberEmail} does not exist in {groupEmail}");
            }
            else
            {
                _logger.LogError("An error occurred while updating Google: delete {memberEmail} from {groupEmail}", memberEmail, groupEmail);
                RequestErrorLogging(resultResponse);

                throw new Exception($"An error occurred while updating Google. {resultResponse.ReasonPhrase} {resultResponse.Content}");
            }
        }

        var group = await GetCachedGroupAsync(groupEmail).ConfigureAwait(false);
        if (group != null)
        {
            group.Remove(memberEmail);
        }
    }

    public async Task<IList<Member>> GetAllMembersAsync(string groupEmail, bool bypassCache = false, bool includedNestedMembers = false)
    {
        var group = await GetCachedGroupAsync(groupEmail).ConfigureAwait(false);

        // get a new copy if it isn't cached, calling code wants a fresh copy, or we are getting the nested members
        var getFreshCopy = (group == null || bypassCache || includedNestedMembers);

        // cache the fresh copy only if nestedMembers were NOT returned
        var cacheFreshCopy = !includedNestedMembers;

        if (getFreshCopy)
        {
            var members = await GetMembersFromGoogleAsync(groupEmail,includedNestedMembers).ConfigureAwait(false);

            if (cacheFreshCopy)
            {
                group = new SortedList<string, Member>(members.ToDictionary(s => s.Email));
                await SetCachedGroupAsync(groupEmail, group);
            }

            // return fresh copy
            return members;
        }

        // return cached copy
        return group.Values.ToList();
    }

    public async Task<Member> GetMemberAsync(string groupEmail, string memberEmail, bool bypassCache = false)
    {
        var group = await GetCachedGroupAsync(groupEmail).ConfigureAwait(false);

        if (group == null || bypassCache)
        {
            var members = await GetAllMembersAsync(groupEmail, bypassCache).ConfigureAwait(false);
            group = new SortedList<string, Member>(members.ToDictionary(s => s.Email));
        }

        if (group.ContainsKey(memberEmail))
            return group[memberEmail];
        else
            return null;
    }

    public async Task<Member> UpdateMemberAsync(string groupEmail, Member member)
    {
        // poor man's throttling - convert to synchronous
        var result = _membersClient.UpdateAsync(groupEmail, member.Email, member).GetAwaiter().GetResult();
        var resultResponse = result.GetRawResponse();

        if (resultResponse.IsError)
        {
            //TODO: move this out of an exception to something more graceful and system-wide
            if (resultResponse.Status == (int)System.Net.HttpStatusCode.Conflict)
            {
                _logger.LogError("{memberEmail} does not exist in {groupEmail}", member.Email, groupEmail);
                throw new Exception($"{member.Email} does not exist in {groupEmail}");
            }
            else
            {
                _logger.LogError("An error occurred while updating Google: update {memberEmail} in {groupEmail}", member.Email, groupEmail);
                RequestErrorLogging(resultResponse);

                throw new Exception($"An error occurred while updating Google. {resultResponse.ReasonPhrase} {resultResponse.Content}");
            }
        }

        var returnedMember = result.Value;

        var group = await GetCachedGroupAsync(groupEmail).ConfigureAwait(false);
        if (group != null)
        {
            group[returnedMember.Email] = returnedMember;
        }

        return returnedMember;
    }

    public async Task<IList<Member>> GetAllNestedMembers(string groupEmail)
    {
        return await GetMembersFromGoogleAsync(groupEmail, true).ConfigureAwait(false);
    }

    /// <summary>
    /// Does the actual retrieving of the group members from Google
    /// </summary>
    /// <param name="groupEmail"></param>
    /// <param name="includedNestedMembers"></param>
    /// <returns></returns>
    private async Task<IList<Member>> GetMembersFromGoogleAsync(string groupEmail, bool includedNestedMembers = false)
    {
        var noMorePages = false;
        var pageCount = 1;  // for logging purposes only
        var pageToken = string.Empty;

        var members = new List<Member>();

        Response<Members> result;
        Response resultResponse;
        while (!noMorePages && pageToken != null)
        {
            _logger.LogDebug("Retrieving page {pageCount} {groupEmail} members", pageCount, groupEmail);

            result = await _membersClient.GetAllAsync(groupEmail, pageToken, includeDerivedMembership: includedNestedMembers.ToString()).ConfigureAwait(false);
            resultResponse = result.GetRawResponse();

            if (resultResponse.IsError)
            {
                var responseContent = resultResponse.Content.ToString();
                var errorMessage = $"Error retrieving page {pageCount} of {groupEmail} member list: {resultResponse.Status}: {responseContent}";
                _logger.LogError(errorMessage);
                throw new Exception(errorMessage);
            }

            if (result.Value.MembersProperty == null)
            {
                _logger.LogInformation("MembersProperty null for {groupEmail}", groupEmail);
                //throw new Exception($"MembersProperty null for {groupEmail}");
                return new List<Member>();
            }

            var pageOfMembers = result.Value.MembersProperty;
            members.AddRange(pageOfMembers);

            //if (result.Body.NextPageToken != null)
            pageToken = result.Value.NextPageToken;
            noMorePages = string.IsNullOrEmpty(pageToken);

            pageCount++;
        }

        return members.OrderBy(o => o.Email).ToList();
    }

    /// <summary>
    /// Returns the group's members from the cache, if exists
    /// </summary>
    private Task<SortedList<string, Member>> GetCachedGroupAsync(string groupEmail)
    {
        return _cache.GetAsync(CacheKey(groupEmail));
    }

    private Task SetCachedGroupAsync(string groupEmail, SortedList<string, Member> members)
    {
        return _cache.SetAsync(groupEmail, members, TimeSpan.FromMinutes(5));
    }

    private string CacheKey(string groupEmail)
    {
        return groupEmail;
    }

    private void RequestErrorLogging(Response result)
    {
        _logger.LogError("Response Headers: {responseHeaders}", result.Headers);
        _logger.LogError("Response. Body: {responseContent}", result.Content);
    }
}
