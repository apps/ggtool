using Aae.Caching;
using AutoMapper;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Repositories;

public class GroupRepository : IGroupRepository
{
    private readonly GroupsClient _groupsClient;
    private readonly AppsClient _appsClient;
    private readonly ICache<Group> _cache;
    private readonly IMapper _mapper;

    public GroupRepository(UwGoogleGroupsClient groupsClient, ICache<Group> cache, IMapper mapper)
    {
        _groupsClient = groupsClient?.GetGroupsClient() ?? throw new ArgumentNullException(nameof(groupsClient));
        _appsClient = groupsClient?.GetAppsClient() ?? throw new ArgumentNullException(nameof(groupsClient));
        _cache = cache ?? throw new ArgumentNullException(nameof(cache));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public async Task<Group> AddAsync(Group group)
    {
        var groupToAdd = _mapper.Map<Uw.GoogleGroups.Client.Group>(group);
        var groupResultFromGoogle = await _groupsClient.CreateAsync(groupToAdd).ConfigureAwait(false);
        var groupResultResponseFromGoogle = groupResultFromGoogle.GetRawResponse();

        if (groupResultResponseFromGoogle.IsError)
            throw new Exception("An error occured while adding group Google.");

        // now put in the settings for the newly created group
        var returnedGroup = await UpdateSettingsAsync(group).ConfigureAwait(false);
        _mapper.Map(groupResultFromGoogle.Value, returnedGroup);

        await SetCachedGroupAsync(group.Email, returnedGroup);

        return returnedGroup;
    }

    public async Task<Group> GetAsync(string groupEmail, bool bypassCache = false)
    {
        var group = await GetCachedGroupAsync(groupEmail).ConfigureAwait(false);

        if (group == null || bypassCache)
        {
            // group wasn't cached, or calling code wants a fresh copy from Google
            group = await LoadCacheFromGoogleAsync(groupEmail).ConfigureAwait(false);
        }

        return group;
    }

    public async Task<IList<string>> ListAsync()
    {
        var result = await _appsClient.GetAsync().ConfigureAwait(false);
        return result.Value.ToList();
    }

    public async Task DeleteAsync(string groupEmail)
    {
        await _groupsClient.DeleteGroupAsync(groupEmail).ConfigureAwait(false);
        return;
    }

    public async Task<Group> UpdateAsync(Group group)
    {
        // issues when trying to update both group properties and settings simultaneously (descriptions and names can conflict)
        // Do one at a time. Do settings first, as properties will contain the name and description

        var returnedGroup = await UpdateSettingsAsync(group);

        var groupPropertiesToUpdate = _mapper.Map<Uw.GoogleGroups.Client.Group>(group);
        var propertiesResult = await _groupsClient.UpdatePropertiesAsync(group.Email, groupPropertiesToUpdate).ConfigureAwait(false);
        var propertiesResultResponse = propertiesResult.GetRawResponse();

        if (propertiesResultResponse.IsError)
            throw new Exception("An error occured while updating the group properties.");

        _mapper.Map(propertiesResult.Value, returnedGroup);

        await SetCachedGroupAsync(group.Email, returnedGroup);

        return returnedGroup;
    }

    private async Task<Group> UpdateSettingsAsync(Group group)
    {
        var groupSettingsToUpdate = _mapper.Map<Uw.GoogleGroups.Client.GroupSettings>(group);
        var result = await _groupsClient.UpdateSettingsAsync(group.Email, groupSettingsToUpdate).ConfigureAwait(false);
        var resultResponse = result.GetRawResponse();

        if (resultResponse.IsError)
            throw new Exception("An error occured while updating the group settings.");

        return _mapper.Map<Group>(result.Value);
    }

    /// <summary>
    /// Loads the cache with group members from Google
    /// </summary>
    /// <param name="groupEmail"></param>
    private async Task<Group> LoadCacheFromGoogleAsync(string groupEmail)
    {
        var propertiesTask = _groupsClient.GetPropertiesAsync(groupEmail);
        var settingsTask = _groupsClient.GetSettingsAsync(groupEmail, "json");

        await Task.WhenAll(propertiesTask, settingsTask).ConfigureAwait(false);

        if (propertiesTask.Result == null)
            return null;

        // map the API client models for properties and settings to the local Group
        var group = _mapper.Map<Group>(propertiesTask.Result.Value);
        _mapper.Map(settingsTask.Result.Value, group);

        await SetCachedGroupAsync(groupEmail, group);

        return group;
    }

    /// <summary>
    /// Returns the group's members from the cache, if exists
    /// </summary>
    private Task<Group> GetCachedGroupAsync(string groupEmail)
    {
        return _cache.GetAsync(CacheKey(groupEmail));
    }

    private Task SetCachedGroupAsync(string groupEmail, Group group)
    {
        return _cache.SetAsync(groupEmail, group, TimeSpan.FromMinutes(5));
    }

    private string CacheKey(string groupEmail)
    {
        return groupEmail;
    }
}
