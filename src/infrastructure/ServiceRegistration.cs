using System.Text;
using Aae.Caching;
using AAE.GGTool.Application.Configuration.DomainEvents;
using AAE.GGTool.Application.Interfaces;
using AAE.GGTool.Infrastructure;
using AAE.GGTool.Infrastructure.Contexts;
using AAE.GGTool.Repositories;
using Azure.Core;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool;

public static class ServiceRegistration
{
    /// <summary>
    /// Add services for the application to the specified <see cref="IServiceCollection"/>.
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configure"></param>
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, Action<AppInfrastructureOptions> configure)
    {
        if (services == null)
            throw new ArgumentNullException(nameof(services));

        if (configure == null)
            throw new ArgumentNullException(nameof(configure));

        var options = new AppInfrastructureOptions();
        configure(options);

        if (string.IsNullOrEmpty(options.ConnectionString))
            throw new ArgumentException($"{nameof(options.ConnectionString)} cannot be empty.");

        services.AddTransient(o => options);

        services.AddInfrastructure(connectionString: options.ConnectionString,
                            googleGroupCredential: options.GoogleGroupCredential);

        return services;
    }

    /// <summary>
    /// Adds all application services to the specified <see cref="IServiceCollection"/>.
    /// </summary>
    /// <param name="services"></param>
    /// <param name="connectionString"></param>
    /// <param name="googleGroupCredential"></param>
    public static IServiceCollection AddInfrastructure(
        this IServiceCollection services,
        string connectionString,
        TokenCredential googleGroupCredential)
    {
        // ############# REPOSITORIES ################
        services.AddTransient<IGroupDefaultsRepository, GroupDefaultsRepository>();
        services.AddTransient<IGroupMemberRepository, GroupMemberRepository>();
        services.AddTransient<IGroupRepository, GroupRepository>();

        // ############# CONNECTION FACTORIES ################
        services.AddSingleton<IDbConnectionFactoryGGTool, SqlConnectionFactoryGGTool>(c => new SqlConnectionFactoryGGTool(connectionString));

        // ############# APIs ################
        services.AddAzureClients(clientBuilder =>
        {
            // UW API client
            clientBuilder.AddUwGoogleGroupsClient()
                .WithCredential(googleGroupCredential);
        });

        // needed for ExcelDataReader
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

        services.AddInMemoryCaching();

        services.Scan(scan => scan
            .FromAssemblies(Assemblies.Application, Assemblies.Infrastructure)
                .AddClasses(classes => classes.Where(type => type.Name.EndsWith("Repository")))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime()
                .AddClasses(classes => classes.AssignableTo(typeof(IPipelineBehavior<,>)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime()
                .AddClasses(classes => classes.AssignableTo(typeof(IJob)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime()
                .AddClasses(classes => classes.AssignableTo(typeof(IRequestHandler<,>)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime()
                .AddClasses(classes => classes.AssignableTo(typeof(IDomainEventNotification<>)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());

        services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(connectionString)
        );

        return services;
    }
}
