using Microsoft.Extensions.DependencyInjection;

namespace UW.Services.Ldap
{
    public static class IServiceCollectionExtensions
    {
        /// <summary>
        /// Register the UW LDAP Client
        /// </summary>
        public static IServiceCollection AddUWLdapClient(this IServiceCollection services)
        {
            services.AddTransient<IUWLdapClient, UWLdapClient>();

            return services;
        }
    }
}
