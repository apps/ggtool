﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace UW.Services.Ldap
{
    public interface IUWLdapClient
    {
        Task<IList<UWLdapSearchResult>> SearchAsync(string search);
    }
}