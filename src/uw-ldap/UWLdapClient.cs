using LdapForNet;
using System.Collections.Generic;
using System.Threading.Tasks;
using static LdapForNet.Native.Native;

namespace UW.Services.Ldap
{
    public class UWLdapClient : IUWLdapClient
    {
        public async Task<IList<UWLdapSearchResult>> SearchAsync(string search)
        {
            var allResults = new List<UWLdapSearchResult>();
            var lowerSearch = search.ToLower();

            using (var cn = new LdapConnection())
            {
                cn.Connect("ldap.services.wisc.edu", 389);
                cn.Bind(LdapAuthType.Anonymous, new LdapCredential());

                var entries = await cn.SearchAsync("dc=wisc,dc=edu", $"(&(cn={lowerSearch})(datasource=Payroll))");

                if (entries.Count > 0)
                {
                    foreach (var entry in entries)
                    {
                        allResults.Add(FormatSearchResult(entry));
                    }
                }
                else
                {
                    entries = await cn.SearchAsync("dc=wisc,dc=edu", $"(&(cn={lowerSearch})(datasource=Student))");
                    foreach (var entry in entries)
                    {
                        allResults.Add(FormatSearchResult(entry));
                    }
                }
            }

            return allResults;
        }

        private UWLdapSearchResult FormatSearchResult(LdapEntry result)
        {
            result.DirectoryAttributes["cn"].GetValue<string>();
            return new UWLdapSearchResult()
            {
                cn = result.DirectoryAttributes["cn"].GetValue<string>(),
                sn = result.DirectoryAttributes["sn"].GetValue<string>(),
                givenname = result.DirectoryAttributes["givenname"].GetValue<string>(),
                title = result.DirectoryAttributes["title"].GetValue<string>(),
                mail = result.DirectoryAttributes["mail"].GetValue<string>(),
                wiscedualldepartments = result.DirectoryAttributes["wiscedualldepartments"].GetValue<string>()
            };
        }
    }
}
