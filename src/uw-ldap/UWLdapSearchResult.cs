namespace UW.Services.Ldap
{
    public class UWLdapSearchResult
    {
        public string cn { get; set; } = string.Empty;
        public string sn { get; set; } = string.Empty;
        public string givenname { get; set; } = string.Empty;
        public string mail { get; set; } = string.Empty;
        public string title { get; set; } = string.Empty;
        public string wiscedualldepartments { get; set; } = string.Empty;
    }
}
