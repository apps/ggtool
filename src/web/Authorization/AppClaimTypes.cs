﻿namespace AAE.GGTool.Web.Authorization
{
    public static class AppClaimTypes
    {
        /// <summary>
        /// The custom claim type for the user permissions
        /// </summary>
        public const string Permissions = "https://aae.wisc.edu/claims/permissions";
    }
}
