﻿namespace AAE.GGTool.Web.Authorization
{
    /// <summary>
    /// Config object holding values for app groups from appsettings
    /// </summary>
    public class AppGroupConfig
    {
        public string SysAdmin { get; set; }
        public string SiteManagers { get; set; }
        public string SiteReadOnly { get; set; }
    }
}
