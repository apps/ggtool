using UW.Shibboleth;

namespace AAE.GGTool.Web.Authorization;

public static class AuthenticationExtensions
{
/// <summary>
/// Generates an list of user-defined attributes to mock a Shibboleth session for the specified username.
/// </summary>
/// <param name="username">Username of user to impersonate - valid values: ewdieckman, eewall, white24.</param>
/// <returns></returns>
public static ShibbolethAttributeValueCollection GenerateAppDevAttributes(string username)
{
    // setup a dictionary with the various users setup
    var authDictionary = new Dictionary<string, ShibbolethAttributeValueCollection>
    {
        // ewdieckman - SysAdmin
        {
            "ewdieckman",
            new ShibbolethAttributeValueCollection()
            {
                new ShibbolethAttributeValue("uid", "ewdieckman"),
                new ShibbolethAttributeValue("givenName", "Eric"),
                new ShibbolethAttributeValue("sn", "Dieckman"),
                new ShibbolethAttributeValue("mail", "eric.dieckman@wisc.edu"),
                new ShibbolethAttributeValue("wiscEduPVI", "UW106Z480"),
                new ShibbolethAttributeValue("isMemberOf", "uw:domain:aae.wisc.edu:it:sysadmin;uw:domain:aae.wisc.edu:administrativestaff"),
            }
        },

        // eewall - site manager
        {
            "eewall",
            new ShibbolethAttributeValueCollection()
            {
                new ShibbolethAttributeValue("uid", "eewall"),
                new ShibbolethAttributeValue("givenName", "Erin"),
                new ShibbolethAttributeValue("sn", "Wall"),
                new ShibbolethAttributeValue("mail", "erin.wall@wisc.edu"),
                new ShibbolethAttributeValue("wiscEduPVI", "UW834D494"),
                new ShibbolethAttributeValue("isMemberOf", "uw:domain:aae.wisc.edu:apps:ggtool:site-managers"),
            }
        },

        // white24 - site readonly
        {
            "white24",
            new ShibbolethAttributeValueCollection()
            {
                new ShibbolethAttributeValue("uid", "white24"),
                new ShibbolethAttributeValue("givenName", "Jo"),
                new ShibbolethAttributeValue("sn", "White"),
                new ShibbolethAttributeValue("mail", "jo.white@wisc.edu"),
                new ShibbolethAttributeValue("wiscEduPVI", "UW658A474"),
                new ShibbolethAttributeValue("isMemberOf", "uw:domain:aae.wisc.edu:administrativestaff;uw:domain:aae.wisc.edu:apps:ggtool:site-readonly"),
            }
        },

        // sjohnston3 - general users
        {
            "sjohnston3",
            new ShibbolethAttributeValueCollection()
            {
                new ShibbolethAttributeValue("uid", "sjohnston3"),
                new ShibbolethAttributeValue("givenName", "Sarah"),
                new ShibbolethAttributeValue("sn", "Johnston"),
                new ShibbolethAttributeValue("mail", "sarah.johnston@wisc.edu"),
                new ShibbolethAttributeValue("wiscEduPVI", "UW525C550")
            }
        },
    };

    // check for the specified user
    if (!authDictionary.TryGetValue(username, out ShibbolethAttributeValueCollection? attributes))
        throw new Exception($"{username} is not a valid dev authentication name");

    return attributes;
}
}
