using AAE.GGTool.Web.Authorization.PolicyProvider;
using AAE.GGTool.Web.Security;
using Microsoft.AspNetCore.Authorization;

namespace AAE.GGTool.Web.Authorization
{
    public static class AuthorizationExtensions
    {
        /// <summary>
        /// Adds authorization services to the specified <see cref="IServiceCollection" />.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddPermissionsAuthorization(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddAuthorization(options =>
            {
                //options.AddPolicy(Policies.PolicyName, policy => policy.Requirements.Add(new SomePermission()));

                // global access policy
                options.AddPolicy(Policies.RequireAtLeastOneAppGroup,
                    policyBuilder => policyBuilder.AddRequirements(
                        new AccessAppPermission()
                        ));
            });

            services.AddSingleton<IAuthorizationHandler, AccessAppHandler>();
            services.AddSingleton<IAuthorizationHandler, PermissionHandler>();

            // permissions based stuff
            services.AddScoped<IUserPermissionService, UserPermissionService>();

            // Overrides the DefaultAuthorizationPolicyProvider with our own
            // https://github.com/dotnet/aspnetcore/blob/main/src/Security/Authorization/Core/src/DefaultAuthorizationPolicyProvider.cs
            services.AddSingleton<IAuthorizationPolicyProvider, PermissionAuthorizationPolicyProvider>();

            return services;
        }

        /// <summary>
        /// Adds authorization services to the specified <see cref="IServiceCollection" />. Also initializes the app groups
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="appGroupConfig"></param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddAuthorization(this IServiceCollection services, AppGroupConfig appGroupConfig)
        {
            var app_groups = new AppGroups(
                sysadmin: appGroupConfig.SysAdmin,
                siteManagers: appGroupConfig.SiteManagers,
                siteReadOnly: appGroupConfig.SiteReadOnly);
            services.AddSingleton(app_groups);

            services.AddPermissionsAuthorization();

            return services;
        }
    }
}
