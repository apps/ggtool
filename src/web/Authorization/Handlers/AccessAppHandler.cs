using AAE.GGTool.Identity;
using AAE.GGTool.Web.Security;
using Microsoft.AspNetCore.Authorization;

namespace AAE.GGTool.Web.Authorization;

/// <summary>
/// Handler for <see cref="AccessAppPermission"/>
/// </summary>
public class AccessAppHandler : AuthorizationHandler<AccessAppPermission>
{
    private readonly AppGroups _appGroups;

    public AccessAppHandler(AppGroups appGroups)
    {
        _appGroups = appGroups;
    }

    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AccessAppPermission requirement)
    {
        var user = new GGToolUser(context.User);

        // get the type of the AppGroups
        var appGroupType = _appGroups.GetType();

        var propertyInfo = appGroupType.GetProperties();

        // now loop all properties and get the value, checking if the user is a member
        foreach (var property in propertyInfo)
        {
            var groupName = property.GetValue(_appGroups)?.ToString();
            if (groupName == null)
                throw new Exception($"{property.Name} value in AppGroups is null");
            if (user.IsMemberOf(groupName))
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }
        }

        // explicitly fail as the nature of this handler is that they MUST be in one of these groups
        context.Fail();
        return Task.CompletedTask;
    }
}
