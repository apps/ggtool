﻿namespace AAE.GGTool.Web.Authorization
{
    public static class Permissions
    {
        // Separate actions from subjects with a "." in order to parse for use in CASL rules
        // do NOT use "_" to separate, as those are the separators for the permissions policy provider parsing
        public const string DeleteGroup = "Delete.Group";
        public const string EditGroupMembership = "Edit.GroupMembership";
        public const string EditGroup = "Edit.Group";
        public const string CreateGroup = "Create.Group";
    }
}
