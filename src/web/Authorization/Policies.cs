﻿namespace AAE.GGTool.Web.Authorization
{
    public static class Policies
    {
        /// <summary>
        /// Allows access when client has at least one of the groups
        /// </summary>
        public const string RequireAtLeastOneAppGroup = "OneAppGroup";
    }
}
