﻿using Microsoft.AspNetCore.Authorization;

// taken from https://github.com/joaopgrassi/authz-custom-middleware/blob/main/src/AuthUtils/PolicyProvider/
namespace AAE.GGTool.Web.Authorization
{
    public enum PermissionOperator
    {
        And = 1,
        Or = 2
    }

    public class PermissionAuthorizeAttribute : AuthorizeAttribute
    {
#pragma warning disable IDE1006 // Naming Styles
        internal const string PolicyPrefix = "PERMISSION_";
#pragma warning restore IDE1006 // Naming Styles
        private const string _separator = "_";

        /// <summary>
        /// Initializes the attribute with multiple permissions
        /// </summary>
        /// <param name="permissionOperator">The operator to use when verifying the permissions provided</param>
        /// <param name="permissions">The list of permissions</param>
        public PermissionAuthorizeAttribute(PermissionOperator permissionOperator, params string[] permissions)
        {
            Policy = PolicyName(permissionOperator, permissions);
        }

        /// <summary>
        /// Initializes the attribute with a single permission
        /// </summary>
        /// <param name="permission">The permission</param>
        public PermissionAuthorizeAttribute(string permission)
        {
            Policy = PolicyName(permission);
        }

        public static PermissionOperator GetOperatorFromPolicy(string policyName)
        {
            var @operator = int.Parse(policyName.AsSpan(PolicyPrefix.Length, 1));
            return (PermissionOperator)@operator;
        }

        public static string[] GetPermissionsFromPolicy(string policyName)
        {
            return policyName.Substring(PolicyPrefix.Length + 2)
                .Split(new[] { _separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string PolicyName(string permission)
        {
            // E.g: PERMISSION_1_Create..
            return $"{PolicyPrefix}{(int)PermissionOperator.And}{_separator}{permission}";
        }

        public static string PolicyName(PermissionOperator permissionOperator, params string[] permissions)
        {
            // E.g: PERMISSION_1_Create_Update..
            return $"{PolicyPrefix}{(int)permissionOperator}{_separator}{string.Join(_separator, permissions)}";
        }
    }
}
