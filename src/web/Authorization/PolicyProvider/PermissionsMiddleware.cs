﻿// taken from https://github.com/joaopgrassi/authz-custom-middleware/blob/main/src/API/Authorization/PermissionsMiddleware.cs
using AAE.GGTool.Identity;

namespace AAE.GGTool.Web.Authorization.PolicyProvider
{
    public class PermissionsMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<PermissionsMiddleware> _logger;

        public PermissionsMiddleware(
            RequestDelegate next,
            ILogger<PermissionsMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, IUserPermissionService permissionService)
        {
            if (context.User.Identity == null || !context.User.Identity.IsAuthenticated)
            {
                await _next(context);
                return;
            }

            var cancellationToken = context.RequestAborted;

            var permissionsIdentity = await permissionService.GetUserPermissionsIdentity(context.User, cancellationToken);
            if (permissionsIdentity != null)
            {
                // User has permissions, so we add the extra identity containing the "permissions" claims
                context.User.AddIdentity(permissionsIdentity);
            }

            await _next(context);
        }
    }
}
