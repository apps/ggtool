﻿using AAE.GGTool.Identity;
using AAE.GGTool.Web.Security;
using System.Security.Claims;

namespace AAE.GGTool.Web.Authorization.PolicyProvider
{
    public interface IUserPermissionService
    {
        /// <summary>
        /// Returns a new identity containing the user permissions as Claims
        /// </summary>
        /// <param name="principal">The GGTool user</param>
        /// <param name="cancellationToken"></param>
        ValueTask<ClaimsIdentity> GetUserPermissionsIdentity(ClaimsPrincipal principal, CancellationToken cancellationToken);
    }

    public class UserPermissionService : IUserPermissionService
    {
        private readonly AppGroups _appGroups;

        public UserPermissionService(AppGroups appGroups)
        {
            _appGroups = appGroups;
        }

        public ValueTask<ClaimsIdentity> GetUserPermissionsIdentity(ClaimsPrincipal principal, CancellationToken cancellationToken)
        {
            // replaced with a database lookup or permission service lookup with granular permissions

            var userPermissions = new List<Claim>();

            var user = new GGToolUser(principal);

            // get all Permissions
            var fields = typeof(Permissions).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
            var permissionValues = fields.Where(f => f.IsLiteral && !f.IsInitOnly && f.FieldType == typeof(string))
                .Select(f => (string)f.GetRawConstantValue()).ToList();

            // SiteManagers and sysadmins get all permissions
            if (user.IsMemberOf(_appGroups.SiteManagers) || user.IsMemberOf(_appGroups.SysAdmin))
            {
                foreach(var permission in permissionValues)
                {
                    userPermissions.Add(new Claim(AppClaimTypes.Permissions, permission));
                }
            }

            // readonly gets "nothing"

            return ValueTask.FromResult(CreatePermissionsIdentity(userPermissions));
        }

        private static ClaimsIdentity CreatePermissionsIdentity(IReadOnlyCollection<Claim> claimPermissions)
        {
            if (!claimPermissions.Any())
                return null;

            var permissionsIdentity = new ClaimsIdentity(GGToolUser.GGToolPermissionsAuthenticationType);
            permissionsIdentity.AddClaims(claimPermissions);

            return permissionsIdentity;
        }
    }
}
