﻿using Microsoft.AspNetCore.Authorization;

namespace AAE.GGTool.Web.Authorization
{
    /// <summary>
    /// Permission to simply access the app (minimum permission
    /// </summary>
    public class AccessAppPermission : IAuthorizationRequirement
    {
    }
}
