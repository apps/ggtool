﻿import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library, dom } from '@fortawesome/fontawesome-svg-core'

// template
import { faSpinnerThird, faSpinner, faUser, faSignOutAlt, faTrashAlt as fasTrashAlt } from '@fortawesome/pro-solid-svg-icons'
library.add(faSpinnerThird, faSpinner, faUser, faSignOutAlt, fasTrashAlt)

import { faCog, faUsers, faSync, faUserPlus, faFileImport, faFileUpload, faBallPile, faPlus, faUserTimes } from '@fortawesome/pro-solid-svg-icons'
import { faUsersClass, faSearch, faUsersMedical } from '@fortawesome/pro-light-svg-icons'
import { faSave, faTrashAlt, faFileExcel } from '@fortawesome/pro-regular-svg-icons'
import { faGoogle } from '@fortawesome/free-brands-svg-icons'

library.add(faCog, faUsers, faSync, faUserPlus, faFileImport, faFileUpload, faBallPile, faUsersClass, faSearch, faSave, faTrashAlt, faFileExcel, faGoogle, faPlus, faUserTimes, faUsersMedical);

dom.watch();

export default FontAwesomeIcon;
