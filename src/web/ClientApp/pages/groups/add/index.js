import { createApp } from 'vue';
import AddGroup from './app.vue';

const $app = document.getElementById('js-vue-app');

createApp(AddGroup, {
    endpoint: $app.dataset.endpoint,
    editEndpoint: $app.dataset.editendpoint.replace('-999', '{group}'),
    domain: $app.dataset.domain
})
    .mount($app);
