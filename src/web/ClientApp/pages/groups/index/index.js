import { createApp } from 'vue';
import GroupDetail from './app.vue';
import { parseInitialData } from '@/utils/parse_data';
import { abilitiesPlugin } from '@casl/vue';

import { createPinia } from 'pinia'
import { useStore } from '@/stores';


const $app = document.getElementById('js-vue-app');
let initialData = parseInitialData('js-app-initial-data');

// preload the session data into pinia
const pinia = createPinia();
const store = useStore(pinia);
store.sessionUrl = $app.dataset.session;
store.createSession(store.$state);


createApp(GroupDetail, {
    endpoint: $app.dataset.endpoint,
    group: initialData.group
})
    .use(pinia)
    .use(abilitiesPlugin, store.ability)
    .mount($app);
