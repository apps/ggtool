﻿// import all global things
import { createApp, h } from 'vue';
import '@/scss/site.scss';
import 'bootstrap'

import FontAwesomeIcon from '@/components/fontawesome-icons';

createApp({
    render: () => h('div')
}).component('font-awesome-icon', FontAwesomeIcon)
    .mount('#layout_vue')

