﻿import { createApp } from 'vue';
import MultiAddApp from './app.vue';
import { parseInitialData } from '@/utils/parse_data';

const $app = document.getElementById('js-vue-app');
let initialData = parseInitialData('js-app-initial-data');

createApp(MultiAddApp, {
    endpoint: $app.dataset.endpoint,
    searchEndpoint: $app.dataset.searchendpoint,
    groups: initialData.groups
})
    .mount($app);
