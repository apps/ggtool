﻿import { createApp } from 'vue';
import MemberListing from './app.vue';
import { parseInitialData } from '@/utils/parse_data';
import { abilitiesPlugin } from '@casl/vue';

import { createPinia } from 'pinia'
import { useStore } from '@/stores';


const $app = document.getElementById('js-vue-app');
let initialData = parseInitialData('js-app-initial-data');

// preload the session data into pinia
const pinia = createPinia();
const store = useStore(pinia);
store.sessionUrl = $app.dataset.session;
store.createSession(store.$state);


createApp(MemberListing, {
    endpoint: $app.dataset.endpoint,
    addEndpoint: $app.dataset.addendpoint,
    members: initialData.members,
})
    .use(pinia)
    .use(abilitiesPlugin, store.ability)
    .provide('groupKey', initialData.groupKey)
    .provide('groupDomain', initialData.groupDomain)
    .provide('importSample', $app.dataset.importsample)
    .mount($app);
