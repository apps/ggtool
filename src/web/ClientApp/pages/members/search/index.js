import { createApp } from 'vue';
import SearchMember from './app.vue';

const $app = document.getElementById('js-vue-app');

createApp(SearchMember, {
    endpoint: $app.dataset.endpoint,
    deleteEndpoint: $app.dataset.deleteendpoint
})
    .mount($app);
