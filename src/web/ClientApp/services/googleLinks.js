﻿export const groupUrl = (domain, key) => {
    return 'https://groups.google.com/u/1/a/' + domain + '/g/' + key + '/';    
}
export const memberList = (domain, key) => {

    let url = groupUrl(domain, key);
    return url + 'members';
    
};

export const settings = (domain, key) => {

    let url = groupUrl(domain, key);
    return url + 'settings';

};

export default {};
