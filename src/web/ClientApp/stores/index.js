﻿import { defineStore } from 'pinia';
import $http from '@/utils/http';
import { Ability } from '@casl/ability';

export const useStore = defineStore('ability', {
    state: () => ({
        rules: [],
        name: '',
        username: '',
        sessionUrl: ''
    }),
    getters: {
        ability() {
            return new Ability();
        }
    },
    actions: {
        async createSession() {
            const response = await $http.get(this.sessionUrl);
            this.name = response.data.name;
            this.username = response.data.username;
            this.rules = response.data.permissions;
            this.ability.update(this.rules);

        },
        destroySession() {
            this.name = '';
            this.username = '';
            this.rules = [];
        }
    }
})
