﻿import path from 'path';
import glob from 'glob'


const generatePages = () => {
    const manualEntries = {
        main: 'pages/main'
    };

    const ROOT_PATH = path.resolve(__dirname, '../');

    // generate automatic entry points
    const autoEntriesMap = {};
    const pageEntries = glob.sync('pages/**/index.js', {
        cwd: ROOT_PATH,
    });

    // generates folder1.folder2 from  /folder1/folder2/index.js -
    function generateAutoEntries(path, prefix = '.') {
        const chunkPath = path.replace(/\/index\.js$/, '');
        const chunkName = chunkPath.replace(/\//g, '.');
        autoEntriesMap[chunkName] = `${prefix}/${path}`;
    }

    pageEntries.forEach(path => generateAutoEntries(path));


    return Object.assign(manualEntries, autoEntriesMap);

};

export default generatePages;