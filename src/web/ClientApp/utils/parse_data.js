﻿import DOMPurify from 'dompurify';

export const parseInitialData = (el) => {
    try {
        const initialDataEl = document.getElementById(el);

        return JSON.parse(DOMPurify.sanitize(initialDataEl.textContent).replace(/&quot;/g, '"'));
    } catch (e) {
        console.error(e); // eslint-disable-line no-console

        return {};
    }
};

export default {};
