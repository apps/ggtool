﻿import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';
import generatePages from './utils/generatePages';
const input = generatePages();

console.log(input)

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue({
            template: {
                compilerOptions: {
                    whitespace: 'preserve'
                }
            }
        })
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './'),
            '~': path.resolve(__dirname, './node_modules/')
        },
    },
    build: {
        outDir: '../wwwroot',
        emptyOutDir: true,
        manifest: true,
        cssCodeSplit: false,
        rollupOptions: {
            input
        }
    },
    server: {
        hmr: {
            protocol: 'ws'
        }
    }
})