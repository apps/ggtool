﻿using System.Threading.Tasks;
using AAE.GGTool.Web.ViewModels;
using GitLabApiClient;
using Microsoft.AspNetCore.Mvc;

namespace AAE.GGTool.Web.Controllers
{
    [Route("about")]
    public class AboutController : Controller
    {
        private readonly IGitLabClient _gitLabClient;
        public AboutController(IGitLabClient gitLabClient)
        {
            _gitLabClient = gitLabClient;
        }
        public async Task<IActionResult> Index()
        {
            var releases = await _gitLabClient.Releases.GetAsync("apps/ggtool");
            var vm = new AboutIndexViewModel()
            {
                Releases = releases
            };
            //return Ok(releases);
            return View(vm);
        }
    }
}
