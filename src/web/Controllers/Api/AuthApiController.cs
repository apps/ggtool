using AAE.GGTool.Identity;
using AAE.GGTool.Web.Authorization.PolicyProvider;
using AAE.GGTool.Web.Identity;
using AAE.GGTool.Web.Security;
using Microsoft.AspNetCore.Mvc;

namespace AAE.GGTool.Web.Controllers.Api;

[Route("api/auth")]
[ApiController]
public class AuthApiController : ControllerBase
{
    private readonly AppGroups _appGroups;
    public AuthApiController(AppGroups appGroups)
    {
        _appGroups = appGroups;
    }

    // GET: api/auth/groups
    /// <summary>
    /// Returns a listing of all system groups, and all groups that the logged-in user is a member of
    /// </summary>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [HttpGet("groups", Name = "Auth_Groups")]
    public IActionResult GetGroups()
    {
        var user = new GGToolUser(HttpContext.User);

        return Ok(new { system = _appGroups, user = user.Groups });
    }

    [ProducesResponseType(typeof(ClientSession), StatusCodes.Status200OK)]
    [HttpGet("~/session", Name = "Auth_Session")]
    public IActionResult Session()
    {
        var user = new GGToolUser(HttpContext.User);

        var session = new ClientSession()
        {
            Name = user.DisplayName,
            Groups = user.Groups.ToList(),
            Username = user.Username
        };

        foreach (var permission in user.Permissions)
        {
            var rule = permission.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            session.Permissions.Add(new CaslRule()
            {
                Action = rule[0].ToLower(),
                Subject = rule[1]
            });
        }

        return Ok(session);
    }
}
