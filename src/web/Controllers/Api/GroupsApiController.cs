using Aae.Authentication;
using AAE.GGTool.Services;
using AAE.GGTool.Web.Authorization;
using AAE.GGTool.Web.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Web.Controllers.Api;

[Route("api/groups")]
[ApiController]
public class GroupsApiController : Controller
{
    private readonly IGroupService _groupService;
    private readonly IGroupDefaultsService _groupDefaultsService;
    private readonly IGroupMemberService _groupMemberService;
    private readonly IMapper _mapper;

    public GroupsApiController(IGroupService groupService, IGroupDefaultsService groupDefaultsService, IMapper mapper, IGroupMemberService groupMemberService)
    {
        _groupService = groupService ?? throw new ArgumentNullException(nameof(groupService));
        _groupDefaultsService = groupDefaultsService ?? throw new ArgumentNullException(nameof(groupDefaultsService));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _groupMemberService = groupMemberService ?? throw new ArgumentNullException(nameof(groupMemberService));
    }

    // POST /api/groups
    [PermissionAuthorize(Permissions.CreateGroup)]
    [HttpPost]
    public async Task<IActionResult> AddGroup(AddGroupViewModel group)
    {
        // create the new group to add using input data
        var groupToAdd = new Group()
        {
            Email = string.Join("@", group.Email, ViewBag.AppConfig.GroupDomain),
            Name = group.Name,
            Description = group.Description
        };

        // populate default settings
        var defaultSettings = await _groupDefaultsService.GetAsync(1001);
        _mapper.Map(defaultSettings.Defaults, groupToAdd);

        try
        {
            var groupFromGoogle = await _groupService.AddAsync(groupToAdd);

            // now add current user as an Owner
            var currentUser = new ClaimsApplicationUser(HttpContext.User);

            var memberToAdd = new Member()
            {
                Email = currentUser.Email,
                DeliverySettings = GoogleGroupConstants.DeliverySettings.NoEmail,
                Role = GoogleGroupConstants.Role.Owner
            };

            await _groupMemberService.AddOrUpdateAsync(groupFromGoogle.Email, memberToAdd);

            return Ok(groupFromGoogle);
        }
        catch (Exception ex)
        {
            var result = StatusCode(StatusCodes.Status500InternalServerError, new { ex.Message });
            return result;
        }
    }

    // GET /api/groups/aae-faculty
    [HttpGet("{group}")]
    public async Task<IActionResult> Get(string group, [FromQuery(Name = "refresh")] bool forceRefresh)
    {
        var result = await _groupService.GetAsync(string.Join("@", group, ViewBag.AppConfig.GroupDomain), forceRefresh);

        return Ok(result);
    }

    [PermissionAuthorize(Permissions.EditGroup)]
    [HttpPut("{group}")]
    public async Task<IActionResult> Update(string group, Group groupData)
    {
        groupData.Email = string.Join("@", group, ViewBag.AppConfig.GroupDomain);

        var result = await _groupService.UpdateAsync(groupData);

        return Ok(result);
    }

    [PermissionAuthorize(Permissions.DeleteGroup)]
    [HttpDelete("{group}")]
    public async Task<IActionResult> Delete(string group)
    {
        var groupEmail = string.Join("@", group, ViewBag.AppConfig.GroupDomain);
        await _groupService.DeleteAsync(groupEmail);

        return Ok();
    }
}
