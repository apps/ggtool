﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UW.Services.Ldap;

namespace AAE.GGTool.Web.Controllers.Api
{
    [Route("api/ldap")]
    [ApiController]
    public class LdapApiController : ControllerBase
    {
        private readonly IUWLdapClient _ldapClient;
        public LdapApiController(IUWLdapClient ldapClient)
        {
            _ldapClient = ldapClient ?? throw new ArgumentNullException(nameof(ldapClient));
        }

        // GET /api/ldap/search
        [HttpGet]
        public async Task<IActionResult> Search([FromQuery(Name = "q")] string search)
        {
            var result = await _ldapClient.SearchAsync(search);

            return Ok(result);
        }
    }
}
