using AAE.GGTool.Import;
using AAE.GGTool.Services;
using AAE.GGTool.Web.Authorization;
using AAE.GGTool.Web.Helpers;
using AAE.GGTool.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Web.Controllers.Api;

[Route("api/groups/{group}/members")]
[ApiController]
public class MembersApiController : Controller
{
    private readonly IGroupMemberService _memberService;
    private readonly IImportFileProcessor _importFileProcessor;

    public MembersApiController(IGroupMemberService memberService, IImportFileProcessor importFileProcessor)
    {
        _memberService = memberService ?? throw new ArgumentNullException(nameof(memberService));
        _importFileProcessor = importFileProcessor ?? throw new ArgumentNullException(nameof(importFileProcessor));
    }

    // GET /api/groups/aae-faculty/members
    [HttpGet]
    public async Task<IActionResult> List(string group, [FromQuery(Name = "refresh")] bool forceRefresh = false)
    {
        var groupKey = GroupKey(group);

        var members = await _memberService.GetAllAsync(groupKey, forceRefresh);

        return Ok(members);
    }

    // GET /api/groups/aae-faculty/members/export?nested=true
    [HttpGet("export")]
    public async Task<IActionResult> Export(string group, [FromQuery(Name ="nested")] bool includeNestedMembers = false)
    {
        var groupKey = GroupKey(group);

        var members = await _memberService.GetAllAsync(groupKey,true,includeNestedMembers);

        var excelFile = ExcelMemberExport.MemberExport(members);

        var fileDownloadName = $"{group}_{DateTime.Now:yyyy-MM-dd}.xlsx";
        var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        var fileStream = new MemoryStream();
        excelFile.SaveAs(fileStream);
        fileStream.Position = 0;

        var fsr = new FileStreamResult(fileStream, contentType);
        fsr.FileDownloadName = fileDownloadName;

        return fsr;
    }

    [PermissionAuthorize(Permissions.EditGroupMembership)]
    [HttpPost]
    public async Task<IActionResult> Add(string group, AddMemberViewModel data)
    {
        // validation check - can only process when there are members
        if (data.Addresses.Count == 0)
            return BadRequest();

        var groupKey = GroupKey(group);

        var members = new List<Member>();
        string address;
        var deliverySettings = data.SendOnly ? "NONE" : "ALL_MAIL";
        for (var i = 0; i < data.Addresses.Count; i++)
        {
            address = data.Addresses[i];
            if (!string.IsNullOrEmpty(address))
            {
                members.Add(new Member()
                {
                    Email = address.ToLower(),
                    Role = data.Role.ToUpper(),
                    DeliverySettings = deliverySettings
                });
            }
        }

        var allMembers = await _memberService.AddOrUpdateAllAsync(groupKey, members);
        return Ok(allMembers);
    }

    [HttpPost("import")]
    public async Task<IActionResult> Import(string group, [FromForm] ImportFile importFile)
    {
        // process the CSV/Excel file
        // if not validate, spit out a 4xx error
        // if valid, grab current members and do a compare.
        // return the compare result to the user

        byte[] fileData;
        using (var ms = new MemoryStream())
        {
            importFile.data.CopyTo(ms);
            fileData = ms.ToArray();
        }

        var contentType = importFile.data.ContentType;

        // hack for CSV - it comes in incorrectly as application/vnd.ms-excel
        var fileInfo = new FileInfo(importFile.data.FileName);
        if (fileInfo.Extension.ToLower() == ".csv")
            contentType = "text/csv";

        var fileIsValid = _importFileProcessor.ValidateFile(fileData, contentType);
        if (!fileIsValid)
            return BadRequest("File must be a valid CSV or Excel file with an column labeled Email");

        var importMembers = _importFileProcessor.ProcessFile(fileData, contentType);

        // if a role was specified, change all members to that role
        if (importFile.role != "USE_FILE")
            importMembers = importMembers.Select(m => { m.Role = importFile.role; return m; }).ToList();

        var existingMembers = await _memberService.GetAllAsync(GroupKey(group));

        // compare
        var compareResult = existingMembers.Compare(importMembers);

        // return the result
        return Ok(new
        {
            membersFromImport = importMembers,
            compareResult.MembersToAdd,
            MembersToUpdate = compareResult.MembersToUpdate.Select(f => f.Item2).ToList(),
            compareResult.ExtraMembers
        });
    }

    [PermissionAuthorize(Permissions.EditGroupMembership)]
    [HttpPost("batch")]
    public async Task<IActionResult> Batch(string group, BatchMemberViewModel batchImport)
    {
        var groupKey = GroupKey(group);

        Task<IList<Member>> task;
        switch (batchImport.ImportType)
        {
            case MemberImportType.AddOnly:
                task = _memberService.AddAsync(groupKey, batchImport.Members);
                break;
            case MemberImportType.AddOrUpdate:
                task = _memberService.AddOrUpdateAllAsync(groupKey, batchImport.Members);
                break;
            case MemberImportType.Replace:
                task = _memberService.ReplaceAllAsync(groupKey, batchImport.Members);
                break;
            default:
                IList<Member> emptyList = new List<Member>();
                task = Task.FromResult(emptyList);
                break;
        }

        await task;

        return Ok(task.Result);
    }

    [HttpGet("~/api/members/search")]
    public async Task<IActionResult> Search([FromQuery(Name = "q")] string search, [FromQuery(Name = "refresh")] bool forceRefresh = false)
    {
        if (string.IsNullOrEmpty(search))
            return Ok();

        var result = await _memberService.SearchAllAsync(search, forceRefresh);

        return Ok(result);
    }

    [PermissionAuthorize(Permissions.EditGroupMembership)]
    [HttpPost("~/api/members/delete")]
    public async Task<IActionResult> BatchDelete(IList<GroupMemberWithGroup> membersToDelete)
    {
        GroupMemberWithGroup member;
        var tasks = new List<Task>();
        for (var i = 0; i < membersToDelete.Count; i++)
        {
            member = membersToDelete[i];
            tasks.Add(_memberService.DeleteAsync(member.Group, member.Email));
        }

        await Task.WhenAll(tasks);

        return NoContent();
    }

    [PermissionAuthorize(Permissions.EditGroupMembership)]
    [HttpPost("~/api/members/multiadd")]
    public async Task<IActionResult> MultiAdd(AddMemberWithGroupsViewModel member)
    {
        string address;
        var deliverySettings = member.SendOnly ? "NONE" : "ALL_MAIL";
        var tasks = new List<Task>();
        for (var i = 0; i < member.Addresses.Count; i++)
        {
            address = member.Addresses[i];
            if (!string.IsNullOrEmpty(address))
            {
                for (var j = 0; j < member.Groups.Count; j++)
                {
                    tasks.Add(_memberService.AddOrUpdateAsync(GroupKey(member.Groups[j]), new Member()
                    {
                        Email = address.ToLower(),
                        Role = member.Role.ToUpper(),
                        DeliverySettings = deliverySettings
                    }));
                }
            }
        }

        await Task.WhenAll(tasks);

        return Ok();
    }

    private string GroupKey(string group)
    {
        return $"{group}@{ViewBag.AppConfig.GroupDomain}";
    }
}
