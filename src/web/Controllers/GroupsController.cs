﻿using AAE.GGTool.Services;
using AAE.GGTool.Web.Authorization;
using AAE.GGTool.Web.Helpers.Vue;
using AAE.GGTool.Web.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AAE.GGTool.Web.Controllers
{
    [Route("g")]
    public class GroupsController : Controller
    {
        private readonly IVueParser _vueParser;
        private readonly IMapper _mapper;
        private readonly IGroupService _groupService;
        public GroupsController(IVueParser vueParser, IMapper mapper, IGroupService groupService)
        {
            _vueParser = vueParser ?? throw new ArgumentNullException(nameof(vueParser));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _groupService = groupService ?? throw new ArgumentNullException(nameof(groupService));
        }

        [HttpGet("{group}")]
        public async Task<IActionResult> Index(string group, [FromQuery(Name = "refresh")] bool forceRefresh)
        {
            var groupObj = await _groupService.GetAsync(string.Join("@", group, ViewBag.AppConfig.GroupDomain), forceRefresh);

            var vm = new GroupDetailViewModel()
            {
                Group = groupObj
            };

            vm.VueData = _vueParser.ParseData(vm);

            return View(vm);
        }

        [HttpGet("add")]
        [PermissionAuthorize(Permissions.CreateGroup)]
        public IActionResult Add()
        {
            return View();
        }
    }
}
