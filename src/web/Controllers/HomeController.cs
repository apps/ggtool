﻿using System;
using System.Threading.Tasks;
using AAE.GGTool.Services;
using AAE.GGTool.Web.Helpers.Vue;
using AAE.GGTool.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using UW.Services.Ldap;

namespace AAE.GGTool.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGroupService _groupService;
        private readonly IVueParser _vueParser;
        //private readonly IUWLdapClient _ldapClient;

        public HomeController(IGroupService groupService, IVueParser vueParser)
        {
            _groupService = groupService ?? throw new ArgumentNullException(nameof(groupService));
            _vueParser = vueParser;
        }

        public async Task<IActionResult> Index()
        {
            var groups = await _groupService.ListAsync();

            var vm = new GroupsListViewModel()
            {
                Groups = groups,
                AppId = ViewBag.AppConfig.UWAPIAppId
            };

            vm.VueData = _vueParser.ParseData(vm);

            return View(vm);
        }

        //public async Task<IActionResult> Test()
        //{
        //    var result = await _ldapClient.SearchAsync("eric dieckman");

        //    return Ok(result);
        //}
    }
}
