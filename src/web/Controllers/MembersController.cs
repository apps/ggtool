﻿using AAE.GGTool.Services;
using AAE.GGTool.Web.Authorization;
using AAE.GGTool.Web.Helpers.Vue;
using AAE.GGTool.Web.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AAE.GGTool.Web.Controllers
{
    public class MembersController : Controller
    {
        private readonly IGroupMemberService _groupMemberService;
        private readonly IGroupService _groupService;

        private readonly IVueParser _vueParser;
        private readonly IMapper _mapper;

        public MembersController(IGroupMemberService groupMemberService, IVueParser vueParser, IMapper mapper, IGroupService groupService)
        {
            _vueParser = vueParser ?? throw new ArgumentNullException(nameof(vueParser));
            _groupMemberService = groupMemberService ?? throw new ArgumentNullException(nameof(groupMemberService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _groupService = groupService ?? throw new ArgumentNullException(nameof(groupService));
        }

        // GET /aae-faculty/members
        [HttpGet("g/{group}/members")]
        public async Task<IActionResult> Index(string group)
        {
            var groupEmail = string.Join("@", group, ViewBag.AppConfig.GroupDomain);

            var groupTask = _groupService.GetAsync(groupEmail);

            var membersTask = _groupMemberService.GetAllAsync(groupEmail);

            await Task.WhenAll(groupTask, membersTask);

            var vm = new MemberListViewModel()
            {
                GroupName = groupTask.Result.Name,
                GroupEmail = groupTask.Result.Email,
                GroupDescription = groupTask.Result.Description,
                Members = membersTask.Result
            };

            vm.VueData = _vueParser.ParseData(vm);

            return View(vm);
        }

        // GET /search
        [HttpGet("search")]
        public IActionResult Search()
        {
            return View();
        }

        [PermissionAuthorize(Permissions.EditGroupMembership)]
        [HttpGet("members/addmultiple")]
        public async Task<IActionResult> AddMultiple()
        {
            var groups = await _groupService.ListAsync();
            var vm = new MultiAddViewModel()
            {
                Groups = groups
            };

            vm.VueData = _vueParser.ParseData(vm);
            return View(vm);
        }
    }
}
