﻿using AAE.Helpers.Email;
using AAE.GGTool.Web.Authorization;

namespace AAE.GGTool.Web.Helpers
{
    /// <summary>
    /// Class to hold app config options from appsettings
    /// </summary>
    public class AppConfig
    {
        public AppConfig()
        {
            EmailSettings = new EmailConfig();
            Groups = new AppGroupConfig();
        }
        public string Branding { get; set; }
        public EmailConfig EmailSettings { get; set; }
        public AppGroupConfig Groups { get; set; }
        public string GroupDomain { get; set; }
        public string UWAPIAppId { get; set; }
    }
}
