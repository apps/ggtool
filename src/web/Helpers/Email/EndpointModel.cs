﻿namespace AAE.Helpers.Errors
{
    public class EndpointModel
    {
        public string DisplayName { get; set; }
        public string RoutePattern { get; set; }
        public int? Order { get; set; }
        public string HttpMethods { get; set; }
    }
}
