﻿// stack and error methods taken from ExceptionDetailsProvider
// https://github.com/dotnet/aspnetcore/blob/dcd32c0d14e1745eebf737bf9f948de309e02027/src/Shared/StackTrace/ExceptionDetails/ExceptionDetailsProvider.cs#L14

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Primitives;
using MimeKit;

namespace AAE.Helpers.Errors
{
    /// <summary>
    /// Generates an email from the provided exception model
    /// </summary>
    public static class ErrorEmailCreator
    {
        private const string _fontFamily = "font-family: 'Segoe UI', Tahoma, Arial, Helvetica, sans-serif;";
        private const string _sourceCode = "font-family: Consolas, 'Courier New', courier, monospace;white-space: pre;color:#606060;font-size:.813em;";
        private const string _fontSizeStackTrace = "font-size: .9em;";

        public static MimeEntity CreateStatusCodeEmail(ErrorEmailModel model)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbHtml = new StringBuilder();

            // header section
            var emailHeader = CreateEmailHeader(model.IpAddress, model.UserId, model.Uri, $"A {model.StatusCode} error");
            sb.AppendLine(emailHeader[0]);
            sbHtml.AppendLine(emailHeader[1]);

            // status code section
            sb.AppendLine($"Status Code: {model.StatusCode}");
            sbHtml.Append($"<h2>Status Code: {model.StatusCode}</h2>");

            // Querystring section
            var querySection = CreateQuerySection(model.Query);
            sb.Append(querySection[0]);
            sbHtml.Append(querySection[1]);

            // Request Headers section
            var requestSection = CreateHeadersSection(model.Headers);
            sb.Append(requestSection[0]);
            sbHtml.Append(requestSection[1]);

            // Routing section
            var routingSection = CreateRoutingSection(model.Endpoint, model.RouteValues);
            sb.Append(routingSection[0]);
            sbHtml.Append(routingSection[1]);

            var builder = new BodyBuilder();

            builder.TextBody = sb.ToString();
            builder.HtmlBody = sbHtml.ToString();

            return builder.ToMessageBody();
        }

        public static MimeEntity CreateExceptionEmail(ErrorEmailModel model)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbHtml = new StringBuilder();

            // header section
            var emailHeader = CreateEmailHeader(model.IpAddress, model.UserId, model.Uri, "An error");

            sb.AppendLine(emailHeader[0]);
            sbHtml.AppendLine(emailHeader[1]);

            if (model.ErrorDetails != null)
            {
                // Stacktrace section
                foreach (var errorDetail in model.ErrorDetails)
                {
                    sb.AppendLine($"{errorDetail.Error.GetType().Name}: {errorDetail.Error.Message}");
                    sbHtml.Append($"<h2>{errorDetail.Error.GetType().Name}: {errorDetail.Error.Message}</h2>");

                    string location = string.Empty;
                    var firstFrame = errorDetail.StackFrames.FirstOrDefault();
                    if (firstFrame != null)
                    {
                        location = firstFrame.Function;
                    }

                    if (!string.IsNullOrEmpty(location) && firstFrame != null && !string.IsNullOrEmpty(firstFrame.File))
                    {
                        sb.AppendLine($"{location} in {System.IO.Path.GetFileName(firstFrame.File)}, line {firstFrame.Line}");
                        sbHtml.Append($"<p>{location} in <code>{System.IO.Path.GetFileName(firstFrame.File)}</code>, line {firstFrame.Line}</p>");
                    }
                    else if (!string.IsNullOrEmpty(location))
                    {
                        sb.AppendLine($"{location}");
                        sbHtml.Append($"<p>{location}</p>");
                    }
                    else
                    {
                        sb.AppendLine($"Unknown location");
                        sbHtml.Append($"<p>Unknown location</p>");
                    }
                    foreach (var frame in errorDetail.StackFrames)
                    {
                        if (string.IsNullOrEmpty(frame.File))
                        {
                            sb.AppendLine($"{frame.Function}");
                            sbHtml.Append($"<div style=\"{_fontFamily}{_fontSizeStackTrace}\">{frame.Function}</div>");
                        }
                        else
                        {
                            sb.AppendLine($"{frame.Function} in {System.IO.Path.GetFileName(frame.File)}");
                            sbHtml.Append($"<div style=\"{_fontFamily}{_fontSizeStackTrace}\">{frame.Function} in <code title=\"{ frame.File}\">{System.IO.Path.GetFileName(frame.File)}</code></div>");
                        }

                        if (frame.Line != 0 && frame.ContextCode.Any())
                        {
                            if (frame.PreContextCode.Any())
                            {
                                sbHtml.Append($"<ol start=\"{frame.PreContextLine}\" style=\"list-style: none;\">");
                                foreach (var line in frame.PreContextCode)
                                {
                                    sb.AppendLine(line);
                                    sbHtml.Append($"<li style=\"{_sourceCode}\"><span>{line}</span></li>");
                                }
                                sbHtml.Append("</ol>");
                            }

                            sbHtml.Append($"<ol start=\"{frame.Line}\" style=\"list-style: none;\">");
                            foreach (var line in frame.ContextCode)
                            {
                                sb.AppendLine($"*{line}");
                                sbHtml.Append($"<li style=\"{_sourceCode}\"><span style=\"color:#f00\">{line}</span></li>");
                            }
                            sbHtml.Append("</ol>");

                            if (frame.PostContextCode.Any())
                            {
                                sbHtml.Append($"<ol start=\"{frame.Line + 1}\" style=\"list-style: none;\">");
                                foreach (var line in frame.PostContextCode)
                                {
                                    sb.AppendLine(line);
                                    sbHtml.Append($"<li style=\"{_sourceCode}\"><span>{line}</span></li>");
                                }
                                sbHtml.Append("</ol>");
                            }
                        }
                    }
                }
            }

            // Querystring section
            var querySection = CreateQuerySection(model.Query);
            sb.Append(querySection[0]);
            sbHtml.Append(querySection[1]);

            // Request Headers section
            var requestSection = CreateHeadersSection(model.Headers);
            sb.Append(requestSection[0]);
            sbHtml.Append(requestSection[1]);

            // Routing section
            var routingSection = CreateRoutingSection(model.Endpoint, model.RouteValues);
            sb.Append(routingSection[0]);
            sbHtml.Append(routingSection[1]);

            var builder = new BodyBuilder();

            builder.TextBody = sb.ToString();
            builder.HtmlBody = sbHtml.ToString();

            return builder.ToMessageBody();
        }

        /// <summary>
        /// Creates the header section in the email
        /// </summary>
        /// <param name="ip">Remote IP of the user</param>
        /// <param name="user">Username of the user</param>
        /// <param name="uri">Uri that generated the error</param>
        /// <param name="errorType">Description text of what the error is</param>
        /// <returns></returns>
        private static string[] CreateEmailHeader(string ip, string user, Uri uri, string errorType)
        {
            StringBuilder sb = new StringBuilder($"{errorType} occurred at {DateTime.Now:ddd, MMM d, yyyy h:mmtt} on {uri}. \r\n \r\n");
            StringBuilder sbHtml = new StringBuilder($"<div style=\"{_fontFamily}\">{errorType} occurred at {DateTime.Now:ddd, MMM d, yyyy h:mmtt} on <strong>{uri}</strong><br /><br />");

            sb.AppendLine($"User: {user}");
            sb.AppendLine($"IP: {ip}");

            sbHtml.AppendLine($"User: <strong>{user}</strong><br />");
            sbHtml.AppendLine($"IP: <strong>{ip}</strong></div>");

            return new string[] { sb.ToString(), sbHtml.ToString() };
        }

        /// <summary>
        /// Creates the Querystring section from the request querystring
        /// </summary>
        /// <returns>A 2 element array holding the strings generated
        /// 0 = plain text
        /// 1 = html
        /// </returns>
        private static string[] CreateQuerySection(IQueryCollection query)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbHtml = new StringBuilder();

            sb.AppendLine("------------------------------------");
            sb.AppendLine("Querystring Information");
            sbHtml.Append("<hr />");
            sbHtml.Append("<h3>Querystring Information</h3>");
            if (query.Any())
            {
                foreach (var kv in query.OrderBy(kv => kv.Key))
                {
                    foreach (var v in kv.Value)
                    {
                        sb.AppendLine($"{kv.Key}  =  {v}");
                        sb.AppendLine($"<div style=\"{_fontFamily}\">{kv.Key}  =  {v}</div>");
                    }
                }
            }
            else
            {
                sb.Append(" ~ No querystring information ~");
                sbHtml.Append($"<p style=\"{_fontFamily}\">No querystring information</p>");
            }

            return new string[] { sb.ToString(), sbHtml.ToString() };
        }

        /// <summary>
        /// Creates the headers section from the request headers
        /// </summary>
        /// <returns>A 2 element array holding the strings generated
        /// 0 = plain text
        /// 1 = html
        /// </returns>
        private static string[] CreateHeadersSection(IDictionary<string, StringValues> headers)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbHtml = new StringBuilder();

            sb.AppendLine("------------------------------------");
            sb.AppendLine("Request Headers");
            sbHtml.Append("<hr />");
            sbHtml.Append("<h3>Request Headers</h3>");
            if (headers.Any())
            {
                foreach (var kv in headers.OrderBy(kv => kv.Key))
                {
                    foreach (var v in kv.Value)
                    {
                        sb.AppendLine($"{kv.Key}  =  {v}");
                        sbHtml.AppendLine($"<div style=\"{_fontFamily}\">{kv.Key}  =  {v}</div>");
                    }
                }
            }
            else
            {
                sb.Append(" ~ No request headers ~");
                sbHtml.Append($"<p style=\"{_fontFamily}\">No request headers</p>");
            }

            return new string[] { sb.ToString(), sbHtml.ToString() };
        }

        /// <summary>
        /// Creates the routing section from the request endpoint information and routing values dictionary
        /// </summary>
        /// <returns>A 2 element array holding the strings generated
        /// 0 = plain text
        /// 1 = html
        /// </returns>
        private static string[] CreateRoutingSection(EndpointModel endpoint, RouteValueDictionary routeValues)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbHtml = new StringBuilder();

            sb.AppendLine("------------------------------------");
            sb.AppendLine("Routing Information");
            sbHtml.Append("<hr />");
            sbHtml.Append("<h3>Routing Information</h3>");

            // routing endpoint
            sb.AppendLine("Endpoint");
            sbHtml.Append("<h4>Endpoint</h4>");
            if (endpoint != null)
            {
                sbHtml.Append($"<table style=\"{_fontFamily}\" border=\"1\"><thead><tr><th align=\"left\">Name</th><th align=\"left\">Value</th></tr></thead><tbody><tr><td>Display Name</td><td>{endpoint.DisplayName}</td></tr>");

                if (!string.IsNullOrEmpty(endpoint.RoutePattern))
                {
                    sb.AppendLine($"Route Pattern: {endpoint.RoutePattern}");
                    sbHtml.Append($"<tr><td>Route Pattern</td><td>{endpoint.RoutePattern}</td></tr>");
                }

                if (endpoint.Order != null)
                {
                    sb.AppendLine($"Route Order: {endpoint.Order}");
                    sbHtml.Append($"<tr><td>Route Order</td><td>{endpoint.Order}</td></tr>");
                }

                if (!string.IsNullOrEmpty(endpoint.HttpMethods))
                {
                    sb.AppendLine($"Route HTTP Method: {endpoint.HttpMethods}");
                    sbHtml.Append($"<tr><td>Route HTTP Method</td><td>{endpoint.HttpMethods}</td></tr>");
                }

                sbHtml.Append($"</tbody></table>");
            }
            else
            {
                sb.Append(" ~ No routing endpoint information ~");
                sbHtml.Append($"<p style=\"{_fontFamily}\">No routing endpoint information</p>");
            }

            // routing values
            sb.AppendLine("Route Values");
            sbHtml.Append("<h4>Route Values</h4>");
            if (routeValues != null && routeValues.Any())
            {
                sbHtml.Append($"<table style=\"{_fontFamily}\" border=\"1\"><thead><tr><th align=\"left\">Variable</th><th align=\"left\">Value</th></tr></thead>");

                foreach (var kv in routeValues.OrderBy(kv => kv.Key))
                {
                    sbHtml.Append($"<tr><td>{kv.Key}</td><td>{kv.Value}</td></tr>");
                }
                sbHtml.Append("</table>");
            }
            else
            {
                sb.Append(" ~ No routing values information ~");
                sbHtml.Append($"<p style=\"{_fontFamily}\">No routing values information</p>");
            }

            return new string[] { sb.ToString(), sbHtml.ToString() };
        }
    }
}
