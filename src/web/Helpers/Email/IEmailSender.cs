﻿using System.Threading.Tasks;
using MimeKit;

namespace AAE.Helpers.Email
{
    public interface IEmailSender
    {
        /// <summary>
        /// Send an email to the application's administrator
        /// </summary>
        /// <param name="subject">Subject line of the email</param>
        /// <param name="body">Body of the message in <see cref="MimeEntity"/> format</param>
        Task SendAdminEmailAsync(string subject, MimeEntity body);

        /// <summary>
        /// Send an email to the application's administrator
        /// </summary>
        /// <param name="subject">Subject line of the email</param>
        /// <param name="textMessage">Body of the message in plain text</param>
        /// <param name="htmlMessage">Optional - body of the message in html</param>
        Task SendAdminEmailAsync(string subject, string textMessage, string htmlMessage = "");
    }
}
