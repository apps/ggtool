using OfficeOpenXml;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Web.Helpers;

public static class ExcelMemberExport
{
    public static ExcelPackage MemberExport(IList<Member> members)
    {
        ExcelPackage ExcelPkg = new ExcelPackage();
        ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");

        // headers
        ExcelRange cell;
        cell = wsSheet1.Cells[1, 1];
        cell.Value = "MEMBER";
        cell.Style.Font.Bold = true;

        cell = wsSheet1.Cells[1, 2];
        cell.Value = "ROLE";
        cell.Style.Font.Bold = true;

        // members
        for (var i = 0; i < members.Count; i++)
        {
            var member = members[i];
            cell = wsSheet1.Cells[i + 2, 1];
            cell.Value = member.Email;

            cell = wsSheet1.Cells[i + 2, 2];
            cell.Value = member.Role;
        }

        // autofit columsn
        wsSheet1.Cells.AutoFitColumns();

        return ExcelPkg;
    }
}
