using System.Collections.Generic;

namespace AAE.GGTool.Web.Helpers.Vue
{
    public interface IVueParser
    {
        Dictionary<string, object> ParseData<TModel>(TModel model);
    }
}
