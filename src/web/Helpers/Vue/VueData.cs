// https://dev.to/proticm/how-to-integrate-vue-with-asp-net-mvc---the-synergy-between-the-two-1hl9
using System;
using System.Runtime.CompilerServices;

namespace AAE.GGTool.Web.Helpers.Vue
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class VueData : Attribute
    {
        public VueData([CallerMemberName] string name = null)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
