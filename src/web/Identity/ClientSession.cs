using AAE.GGTool.Web.Authorization.PolicyProvider;

namespace AAE.GGTool.Web.Identity
{
    /// <summary>
    /// Data about the session for client-side consumption
    /// </summary>
    public class ClientSession
    {
        public ClientSession()
        {
            Groups = new List<string>();
            Permissions = new List<CaslRule>();
        }

        public string Name { get; set; }
        public string? Username { get; set; }
        public IList<string> Groups { get; set; }
        public IList<CaslRule> Permissions { get; set; }
    }
}
