using System.Security.Claims;
using Aae.Authentication;
using AAE.GGTool.Web.Authorization;

namespace AAE.GGTool.Identity;

/// <summary>
/// <see cref="IApplicationUser"/> wrapper for the ClaimsPrincipal
/// </summary>
/// <remarks>Provides field access to claims for all GGTool identities</remarks>
public class GGToolUser : ClaimsApplicationUser, IApplicationUser
{
    public const string GGToolAppAuthenticationType = "GGToolApp";
    public const string GGToolPermissionsAuthenticationType = "GGToolPermissions";

    // fields specific to GGTool
    // public string Xyz { get; set; }
    protected List<string> _permissions;

    public IReadOnlyList<string> Permissions => _permissions.AsReadOnly();

    public GGToolUser(ClaimsIdentity? identity) : base(identity)
    {
        // place to populate GGTool-specific fields from Claims
        //var clXyz = identity.FindFirst(StandardClaimTypes.XYZ);
        //if (clXyz != null) Xyz = clXyz.Value;
    }

    public GGToolUser(ClaimsPrincipal principal)
        : this((ClaimsIdentity?)principal.Identity)
    {
        // permission are in a separate identity
        _permissions = new List<string>();

        // get all permission claims and put them in a list of strings
        IEnumerable<Claim> permissions = principal.FindAll(AppClaimTypes.Permissions);

        if (permissions != null)
        {
            foreach (Claim permission in permissions)
            {
                _permissions.Add(permission.Value);
            }
        }
    }
}
