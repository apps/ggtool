﻿using System;
using AAE.GGTool.Web.Authorization.PolicyProvider;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace AAE.GGTool.Web.Identity
{
    public static class IApplicationBuildingExtensions
    {
        /// <summary>
        /// Enables use of the GGTool user identity
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseAppIdentity(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            app.UseMiddleware<IdentityMiddleware>();
            app.UseMiddleware<PermissionsMiddleware>();
            return app;
        }

        /// <summary>
        /// Enables GGTool user with the given options
        /// </summary>
        public static IApplicationBuilder UseAppIdentity(this IApplicationBuilder app, IdentityUserOptions options)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            app.UseMiddleware<IdentityMiddleware>(Options.Create(options));
            app.UseMiddleware<PermissionsMiddleware>();
            return app;
        }
    }
}
