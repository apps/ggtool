using System.Security.Cryptography.X509Certificates;
using Aae.Caching;
using Aae.Identity;
using AAE.GGTool;
using AAE.GGTool.Web;
using AAE.GGTool.Web.Authorization;
using AAE.GGTool.Web.Helpers;
using AAE.GGTool.Web.Helpers.Vue;
using AAE.GGTool.Web.Identity;
using AAE.Helpers.Email;
using AAE.Helpers.Errors;
using Azure.Identity;
using GitLabApiClient;
using Serilog;
using UW.AspNetCore.Authentication;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((ctx, lc) => lc
    .ReadFrom.Configuration(ctx.Configuration)
    .Enrich.FromLogContext());

ConfigureAzureKeyVault(builder);

ConfigureAuthentication(builder, "ewdieckman");

var appConfig = AppConfigGenerator.Create(builder.Configuration, builder.Environment);
ConfigureViews(builder.Services, appConfig);

builder.Services.AddInMemoryCaching();

builder.Services.AddHttpContextAccessor();

var uwApiCredential = new AaeClientSecretCredential(
    builder.Configuration["UWAPI:ClientId"],
    builder.Configuration["UWAPI:ClientSecret"],
    null,
    new AaeClientSecretCredentialOptions()
    {
        AuthorityHost = AaeAuthorityHosts.AaeHostedUwApiHost
    });

builder.Services.AddApplication();
builder.Services.AddInfrastructure(options =>
{
    options.ConnectionString = builder.Configuration.GetConnectionString("ggtool");
    options.GoogleGroupCredential = uwApiCredential;
});
builder.Services.AddTransient<IVueParser, VueParser>();

// ############# AUTHORIZATION ################
builder.Services.AddAuthorization(appConfig.Groups);

// ############# API CLIENTS ################

builder.Services.AddSingleton(appConfig.EmailSettings);
builder.Services.AddSingleton<IEmailSender, EmailSender>();

builder.Services.AddTransient<IGitLabClient, GitLabClient>(sp => {
    return new GitLabClient(hostUrl: builder.Configuration.GetValue<string>("AAEGitLab:Host"),
    authenticationToken: builder.Configuration.GetValue<string>("AAEGitLab:Token"));
});

var app = builder.Build();

app.UseStaticFiles();

if (!app.Environment.IsDevelopment())
{
    app.UseHsts();
    app.UseExceptionHandler("/error");
    app.UseStatusCodePagesWithReExecute("/error/{0}");
    app.UseExceptionEmailer();
}
else
{
    app.UseStatusCodePages();
}

// AFTER UseStaticFiles, UseExceptionHandler or UseStatusCodePagesWithReExecute but BEFORE UseAuthentication and UseAuthorization
app.UseRouting();

app.UseAuthentication();
app.UseHttpsRedirection();
app.UseResponseCompression();

app.UseSerilogRequestLogging();

app.UseAppIdentity();

app.UseAuthorization();

// must UseEndpoints otherwise the UseSpa extension does not work properly
// https://github.com/dotnet/aspnetcore/blob/8968058c9e5fdfdd1242426a03dc80609997edab/src/Middleware/Spa/SpaServices.Extensions/src/SpaDefaultPageMiddleware.cs#L25
#pragma warning disable ASP0014 // Suggest using top level route registrations
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers()
        .RequireAuthorization(Policies.RequireAtLeastOneAppGroup);
    endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");

    endpoints.MapGet("/debug-config", ctx =>
    {
        var config = (builder.Configuration as IConfigurationRoot).GetDebugView();
        return ctx.Response.WriteAsync(config);
    });
});
#pragma warning restore ASP0014 // Suggest using top level route registrations

if (app.Environment.IsDevelopment())
{
    app.UseSpa(spa =>
    {
        spa.Options.SourcePath = "ClientApp";
        spa.Options.DevServerPort = 3000;
        spa.UseProxyToSpaDevelopmentServer(() =>
        {
            return Task.FromResult(new UriBuilder("http", "localhost", spa.Options.DevServerPort).Uri);
        });
    });
}

app.Run();

// Configures the Azure Key Vault for this app
void ConfigureAzureKeyVault(WebApplicationBuilder builder)
{
    if (!builder.Environment.IsDevelopment())
    {
        var azKeyVaultCertificateDN = builder.Configuration["Azure:KeyVault:SubjectDistinguishedName"];
        var azKeyVaultEndpoint = builder.Configuration["Azure:KeyVault:Endpoint"];

        if (azKeyVaultCertificateDN == null || azKeyVaultEndpoint == null)
            throw new Exception("Azure Key Vault configuration missing.");

        using var x509Store = new X509Store(StoreLocation.LocalMachine);

        x509Store.Open(OpenFlags.ReadOnly);

        X509Certificate2Collection certs = x509Store.Certificates;
        var distinguishedName = new X500DistinguishedName(azKeyVaultCertificateDN!);
        IEnumerable<X509Certificate2> certFound = certs
            .Find(
                X509FindType.FindBySubjectDistinguishedName,
                distinguishedName.Name,
                validOnly: false)
            .OfType<X509Certificate2>();

        if (!certFound.Any())
            throw new Exception("Unable to find the certificate to authenticate and access Azure Key Vault");

        builder.Configuration.AddAzureKeyVault(
            new Uri(azKeyVaultEndpoint!),
            new ClientCertificateCredential(
                builder.Configuration["Azure:AD:DirectoryId"],
                builder.Configuration["Azure:AD:ApplicationId"],
                certFound.Single()));
    }
}

// Configures authentication for the app
void ConfigureAuthentication(WebApplicationBuilder builder, string devAuthenticationUsername)
{
    //authentication with AuthenticationExtensions
    builder.Services.AddAuthentication(options =>
    {
        options.DefaultScheme = ShibbolethDefaults.AuthenticationScheme;
    }).AddUWShibboleth(options =>
    {
        if (builder.Environment.IsDevelopment())
        {
            options.Events = new ShibbolethEvents
            {
                OnSelectingProcessor = ctx =>
                {
                    ctx.Processor = new ShibbolethDevelopmentProcessor(AuthenticationExtensions.GenerateAppDevAttributes("ewdieckman"));
                    return Task.CompletedTask;
                },
            };
        }
    });
}

// Add controllers and views with the app config filter.  Sets defaults for routing options
void ConfigureViews(IServiceCollection services, AppConfig appConfig)
{
    services.AddControllersWithViews(options =>
    {
        options.Filters.Add(new AppConfigActionFilter(appConfig));
    });

    services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
    services.Configure<RouteOptions>(options => options.AppendTrailingSlash = true);

    services.AddResponseCompression(options => options.EnableForHttps = true);
}

