﻿namespace AAE.GGTool.Web.Security
{
    /// <summary>
    /// System groups
    /// </summary>
    public class AppGroups
    {
        public AppGroups(string sysadmin, string siteManagers, string siteReadOnly)
        {
            SysAdmin = sysadmin;
            SiteManagers = siteManagers;
            SiteReadOnly = siteReadOnly;
        }

        public string SysAdmin { get; private set; }
        public string SiteManagers { get; private set; }
        public string SiteReadOnly { get; private set; }
    }
}
