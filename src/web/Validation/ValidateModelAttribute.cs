﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace AAE.Api.Validation
{
    /// <summary>
    /// Validates a model state and returns a BadRequest for invalid models
    /// </summary>
    /// <remarks>https://www.jerriepelser.com/blog/validation-response-aspnet-core-webapi/</remarks>
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new ValidationFailedResult(context.ModelState);
            }
        }
    }
}
