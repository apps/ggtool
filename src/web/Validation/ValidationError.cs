﻿
namespace AAE.Api.Validation
{
    /// <summary>
    /// Response returned when a validation error occurs
    /// </summary>
    /// <remarks>https://www.jerriepelser.com/blog/validation-response-aspnet-core-webapi/</remarks>
    public class ValidationError
    {
        public string Field { get; }

        public string Message { get; }

        public ValidationError(string field, string message)
        {
            Field = field != string.Empty ? field : null;
            Message = message;
        }
    }
}
