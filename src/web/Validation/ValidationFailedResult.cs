﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace AAE.Api.Validation
{
    /// <summary>
    /// Action result that is returned for failed validation
    /// </summary>
    /// <remarks>https://www.jerriepelser.com/blog/validation-response-aspnet-core-webapi/</remarks>
    public class ValidationFailedResult : ObjectResult
    {
        public ValidationFailedResult(ModelStateDictionary modelState)
            : base(new ValidationResultModel(modelState))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }
    }
}
