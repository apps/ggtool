﻿using System.Collections.Generic;
using GitLabApiClient.Models.Releases.Responses;

namespace AAE.GGTool.Web.ViewModels
{
    public class AboutIndexViewModel
    {
        public IList<Release> Releases { get; set; }
    }
}
