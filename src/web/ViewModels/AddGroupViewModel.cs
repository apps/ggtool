﻿using System.ComponentModel.DataAnnotations;

namespace AAE.GGTool.Web.ViewModels
{
    /// <summary>
    /// View model for creating a new (or registering an existing) group
    /// </summary>
    public class AddGroupViewModel
    {
        [Required]
        public string Email { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? DefaultsId { get; set; } = default;
    }
}
