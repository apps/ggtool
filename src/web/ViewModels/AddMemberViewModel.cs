﻿using System.Collections.Generic;

namespace AAE.GGTool.Web.ViewModels
{
    /// <summary>
    /// view model for adding members
    /// </summary>
    public class AddMemberViewModel
    {
        public string Role { get; set; } = string.Empty;
        public bool SendOnly { get; set; } = false;
        public IList<string> Addresses { get; set; } = new List<string>();
    }
}
