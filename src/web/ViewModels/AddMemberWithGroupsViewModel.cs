﻿using System.Collections.Generic;

namespace AAE.GGTool.Web.ViewModels
{
    /// <summary>
    /// Model used to add a member to multiple groups
    /// </summary>
    public class AddMemberWithGroupsViewModel : AddMemberViewModel
    {
        public IList<string> Groups { get; set; } = new List<string>();
    }
}
