using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Web.ViewModels;

/// <summary>
/// Viewmodel for sending a batch add/update/delete of members
/// </summary>
public class BatchMemberViewModel
{
    public IList<Member> Members { get; set; } = new List<Member>();

    public MemberImportType ImportType { get; set; }
}
