﻿using AAE.GGTool.Google;
using AAE.GGTool.Web.Helpers.Vue;

namespace AAE.GGTool.Web.ViewModels
{
    public class GroupDetailViewModel : VueViewModel
    {
        [VueData("group")]
        public Group Group { get; set; }
    }
}
