﻿using System.Collections.Generic;
using AAE.GGTool.Web.Helpers.Vue;

namespace AAE.GGTool.Web.ViewModels
{
    /// <summary>
    /// View model for listing all groups authorized for the app
    /// </summary>
    public class GroupsListViewModel : VueViewModel
    {
        [VueData("groups")]
        public IList<string> Groups { get; set; }

        public string AppId { get; set; }
    }
}
