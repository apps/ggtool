﻿using Microsoft.AspNetCore.Http;

namespace AAE.GGTool.Web.ViewModels
{
    /// <summary>
    /// View model for receiving uploads from UI
    /// </summary>
    public class ImportFile
    {
        public string role { get; set; }
        public IFormFile data { get; set; }
    }
}
