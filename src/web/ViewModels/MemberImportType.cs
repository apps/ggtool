﻿namespace AAE.GGTool.Web.ViewModels
{
    /// <summary>
    /// Identifies the type of member import
    /// </summary>
    public enum MemberImportType
    {
        AddOrUpdate = 1,
        Replace = 2,
        AddOnly = 3
    }
}
