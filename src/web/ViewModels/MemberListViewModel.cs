using AAE.GGTool.Web.Helpers.Vue;
using Uw.GoogleGroups.Client;

namespace AAE.GGTool.Web.ViewModels;

/// <summary>
/// View model for listing members page
/// </summary>
public class MemberListViewModel : VueViewModel
{
    public string GroupName { get; set; } = string.Empty;
    public string GroupEmail { get; set; } = string.Empty;
    public string GroupDescription { get; set; } = string.Empty;

    [VueData]
    public string GroupDomain
    {
        get
        {
            return !string.IsNullOrEmpty(GroupEmail) ? GroupEmail.Substring(GroupEmail.IndexOf("@")+1) : "";
        }
    }

    [VueData]
    public string GroupKey
    {
        get
        {
            return !string.IsNullOrEmpty(GroupEmail) ? GroupEmail.Substring(0, GroupEmail.IndexOf("@")) : "";
        }
    }

    [VueData("members")]
    public IList<Member> Members { get; set; } = new List<Member>();
}
