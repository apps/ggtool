﻿using System.Collections.Generic;
using AAE.GGTool.Web.Helpers.Vue;

namespace AAE.GGTool.Web.ViewModels
{
    /// <summary>
    /// View model for use in the multi add page
    /// </summary>
    public class MultiAddViewModel : VueViewModel
    {
        [VueData("groups")]
        public IList<string> Groups { get; set; } = new List<string>();
    }
}
