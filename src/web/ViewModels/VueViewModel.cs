using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AAE.GGTool.Web.ViewModels
{
    /// <summary>
    /// Base class for view models containing initial Vue data
    /// </summary>
    public abstract class VueViewModel
    {
        /// <summary>
        /// Data to be serialized and injected into a Razor view
        /// </summary>
        [JsonIgnore]
        public Dictionary<string, object> VueData { get; set; } = new Dictionary<string, object>();

        /// <summary>
        /// Method to serialize the <see cref="VueData"/> field into JSON
        /// </summary>
        public virtual string VueDataToJson()
        {
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DictionaryKeyPolicy = JsonNamingPolicy.CamelCase
            };

            var json = JsonSerializer.Serialize(VueData, options);
            return json;
        }
    }
}
